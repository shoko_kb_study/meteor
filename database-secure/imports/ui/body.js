import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';


import { Employees } from '../api/employees.js';

import './body.html';
import './employee.js';

var state = new ReactiveDict();

Template.body.onCreated(function bodyOnCreated() {
	Meteor.subscribe('all-employees');
});

Template.search.events({
	'change input' (event, instance) {
		const name = event.target.value;
		state.set('searchByName', name);
	}
});

// Template.employees.onCreated(function employeesOnCreated() {
// //	Meteor.subscribe('all-employees');
// });

Template.employees.helpers({
	employees() {
		const instance = Template.instance();
		const name = state.get('searchByName');
		if (name) {
			// 部分一致
			return Employees.find({name : new RegExp(name, 'i')});
		}
		return Employees.find();
	},
});

Template.employees.events({
	'click .add button.add'(event) {

		event.preventDefault();

		const target = event.target;
		const name = $(target).parents('tr').find('.name').val();
		const age = $(target).parents('tr').find('.age').val();

		if (name && age) {
			// Employees.insert({name : name, age : age}, function () { alert('error'); });
			Meteor.call('employees.insert', name, age);
			$(target).parents('tr').find('.name').val('');
			$(target).parents('tr').find('.age').val('');
		}
	},
});

