import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import { Employees } from '../api/employees.js';

import './body.html';
import './employee.js';

Template.employees.helpers({
	employees() {
		return Employees.find();
	},
});

Template.employees.events({
	'click .add'(event) {

	event.preventDefault();

	const target = event.target;
	const name = $(target).parents('tr').find('.name').val();
	const age = $(target).parents('tr').find('.age').val();
	// console.log(name, age);

	Meteor.call('employees.insert', name, age);

	$(target).parents('tr').find('.name').val('');
	$(target).parents('tr').find('.age').val('');

	// Meteor.call('employees.remove', this._id);
    },
});

