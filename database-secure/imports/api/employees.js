import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Employees = new Meteor.Collection('employees');


if (Meteor.isServer) {
    // 全データを公開する。
    Meteor.publish('all-employees', function () {
      return Employees.find();
    });

    Meteor.methods({
    // 初期化
    'employees.init'() {
        // Employees.remove({});

        // // 決め打ちでデータを登録する。
        // var data = [
        //     { name: 'しゅんぺい', age: '34' },
        //     { name: 'たえこ', age: '33' },
        //     { name: 'こうたろう', age: '4' },
        //     { name: 'ちほ', age: '2' }
        // ];
        // // データをMongoDBに挿入する。
        // data.forEach(function (emp) {
        //     Employees.insert(emp);
        // })
    },
    // 追加
    'employees.insert'(name, age) {
        check(name, String);
        check(age, String);
        //console.log(name, age);
        Employees.insert(
            {name: name, age :age},
            function (error, result) {
                Meteor.call('employees.update_completed', error);
            }
        );
    },
    'employees.insert_completed'(error) {
        if (error) alert('追加失敗');
        //alert(error ? '追加失敗' : '追加成功');
    },
    // 編集
    'employees.update' (id, name, age) {
        const employee = Employees.findOne(id);
        Employees.update(
            id,
            { $set: { name: name, age: age } },
            function (error, result) {
                Meteor.call('employees.update_completed', error);
            }
        );
    },
    'employees.update_completed' (error) {
        if (error) alert('編集失敗');
        // alert(error ? '編集失敗' : '編集成功');
    },
    // 削除
    'employees.remove'(employeeId) {
        check(employeeId, String);
        const employee = Employees.findOne(employeeId);
        Employees.remove(employeeId);
    },
});


}

