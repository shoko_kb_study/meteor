hello = function () {
	if (Meteor.isServer) {
		// サーバから実行
		console.log("hello from server")
	} else {
		// クライアントから実行
		console.log("hello from client")
	}
}