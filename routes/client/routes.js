import { FlowRouter } from 'meteor/kadira:flow-router';

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import App from '../imports/ui/routes.html';

FlowRouter.route('/lists/:_id', {
  name: 'Lists.show',
  action(params, queryParams) {

    // ex. http://localhost:3000/lists/2?query_param1=TEST&query_param2=TEST2

    console.log('[FlowRouter.getRouteName]' + FlowRouter.getRouteName()); // Lists.show

    console.log('[FlowRouter.getParam]' + FlowRouter.getParam('_id')); // 2

    console.log('[FlowRouter.getQueryParam]' + FlowRouter.getQueryParam('query_param1')); // TEST
    console.log('[FlowRouter.getQueryParam]' + FlowRouter.getQueryParam('query_param2')); // TEST2

    console.log('[params._id]' + params._id);  // 2

    console.log("Hello, routes!");
  }
});

Template.App_Info.helpers({
  routeName () { return FlowRouter.getRouteName();},
  id () { return FlowRouter.getParam('_id'); },
  queryParam1 () { return FlowRouter.getQueryParam('query_param1'); },
  queryParam2 () { return FlowRouter.getQueryParam('query_param2'); },
  // params_id () { return params._id; },
});

Template.App_Body.helpers({
  lists : [
    {name : 'TEST1'},
    {name : 'TEST2'}
  ],
  activeListClass(list) {
    const active = ActiveRoute.name('Lists.show') && FlowRouter.getParam('_id') === list._id;
    return active && 'active';
  }
});