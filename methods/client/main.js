import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Meteor.startup(() => {
	// サーバサイド関数を呼び出す。
 	$('#show').on('click', function (){
 		var firstname = $('#firstname').val();
 		var lastname = $('#lastname').val();
	 	Meteor.call('hello', firstname, lastname, function(err, result) {
  			// callback
  			alert(result);
		});
 		
 	});
});

Template.hello.events({
	'click #console'(event, instance) {

		// 可変引数で引数を渡して、callする。
  		Meteor.call('hello', 'Shoko', 'CALL', function(err, result) {
  			// callback
  			console.log(result);
		});

  		// 配列で引数を渡して、callする。
  		Meteor.apply('hello', ['Shoko', 'APPLY'], function (err, result) {
  			console.log(result);
  		});
  	},
});
