import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.events({
	// JQueryが使える。
	'click #alert'(event, instance) {
		alert("clicked sample");
		console.log(instance);
		console.log(instance.$('button#alert'));
		console.log(instance.$('button#alert').text());
	},
});
