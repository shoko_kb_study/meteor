//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var $ = Package.jquery.$;
var jQuery = Package.jquery.jQuery;

(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/dbernhard_jquery-ui-draggable/packages/dbernhard_jquery- //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
(function () {                                                       // 1
                                                                     // 2
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                   //
// packages/dbernhard:jquery-ui-draggable/jquery-ui-draggable.js                                                     //
//                                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                     //
/*! jQuery UI - v1.10.3 - 2013-11-20                                                                                 // 1
* http://jqueryui.com                                                                                                // 2
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.sortable.js                        // 3
* Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */                                           // 4
                                                                                                                     // 5
(function( $, undefined ) {                                                                                          // 6
                                                                                                                     // 7
var uuid = 0,                                                                                                        // 8
	runiqueId = /^ui-id-\d+$/;                                                                                          // 9
                                                                                                                     // 10
// $.ui might exist from components with no dependencies, e.g., $.ui.position                                        // 11
$.ui = $.ui || {};                                                                                                   // 12
                                                                                                                     // 13
$.extend( $.ui, {                                                                                                    // 14
	version: "1.10.3",                                                                                                  // 15
                                                                                                                     // 16
	keyCode: {                                                                                                          // 17
		BACKSPACE: 8,                                                                                                      // 18
		COMMA: 188,                                                                                                        // 19
		DELETE: 46,                                                                                                        // 20
		DOWN: 40,                                                                                                          // 21
		END: 35,                                                                                                           // 22
		ENTER: 13,                                                                                                         // 23
		ESCAPE: 27,                                                                                                        // 24
		HOME: 36,                                                                                                          // 25
		LEFT: 37,                                                                                                          // 26
		NUMPAD_ADD: 107,                                                                                                   // 27
		NUMPAD_DECIMAL: 110,                                                                                               // 28
		NUMPAD_DIVIDE: 111,                                                                                                // 29
		NUMPAD_ENTER: 108,                                                                                                 // 30
		NUMPAD_MULTIPLY: 106,                                                                                              // 31
		NUMPAD_SUBTRACT: 109,                                                                                              // 32
		PAGE_DOWN: 34,                                                                                                     // 33
		PAGE_UP: 33,                                                                                                       // 34
		PERIOD: 190,                                                                                                       // 35
		RIGHT: 39,                                                                                                         // 36
		SPACE: 32,                                                                                                         // 37
		TAB: 9,                                                                                                            // 38
		UP: 38                                                                                                             // 39
	}                                                                                                                   // 40
});                                                                                                                  // 41
                                                                                                                     // 42
// plugins                                                                                                           // 43
$.fn.extend({                                                                                                        // 44
	focus: (function( orig ) {                                                                                          // 45
		return function( delay, fn ) {                                                                                     // 46
			return typeof delay === "number" ?                                                                                // 47
				this.each(function() {                                                                                           // 48
					var elem = this;                                                                                                // 49
					setTimeout(function() {                                                                                         // 50
						$( elem ).focus();                                                                                             // 51
						if ( fn ) {                                                                                                    // 52
							fn.call( elem );                                                                                              // 53
						}                                                                                                              // 54
					}, delay );                                                                                                     // 55
				}) :                                                                                                             // 56
				orig.apply( this, arguments );                                                                                   // 57
		};                                                                                                                 // 58
	})( $.fn.focus ),                                                                                                   // 59
                                                                                                                     // 60
	scrollParent: function() {                                                                                          // 61
		var scrollParent;                                                                                                  // 62
		if (($.ui.ie && (/(static|relative)/).test(this.css("position"))) || (/absolute/).test(this.css("position"))) {    // 63
			scrollParent = this.parents().filter(function() {                                                                 // 64
				return (/(relative|absolute|fixed)/).test($.css(this,"position")) && (/(auto|scroll)/).test($.css(this,"overflow")+$.css(this,"overflow-y")+$.css(this,"overflow-x"));
			}).eq(0);                                                                                                         // 66
		} else {                                                                                                           // 67
			scrollParent = this.parents().filter(function() {                                                                 // 68
				return (/(auto|scroll)/).test($.css(this,"overflow")+$.css(this,"overflow-y")+$.css(this,"overflow-x"));         // 69
			}).eq(0);                                                                                                         // 70
		}                                                                                                                  // 71
                                                                                                                     // 72
		return (/fixed/).test(this.css("position")) || !scrollParent.length ? $(document) : scrollParent;                  // 73
	},                                                                                                                  // 74
                                                                                                                     // 75
	zIndex: function( zIndex ) {                                                                                        // 76
		if ( zIndex !== undefined ) {                                                                                      // 77
			return this.css( "zIndex", zIndex );                                                                              // 78
		}                                                                                                                  // 79
                                                                                                                     // 80
		if ( this.length ) {                                                                                               // 81
			var elem = $( this[ 0 ] ), position, value;                                                                       // 82
			while ( elem.length && elem[ 0 ] !== document ) {                                                                 // 83
				// Ignore z-index if position is set to a value where z-index is ignored by the browser                          // 84
				// This makes behavior of this function consistent across browsers                                               // 85
				// WebKit always returns auto if the element is positioned                                                       // 86
				position = elem.css( "position" );                                                                               // 87
				if ( position === "absolute" || position === "relative" || position === "fixed" ) {                              // 88
					// IE returns 0 when zIndex is not specified                                                                    // 89
					// other browsers return a string                                                                               // 90
					// we ignore the case of nested elements with an explicit value of 0                                            // 91
					// <div style="z-index: -10;"><div style="z-index: 0;"></div></div>                                             // 92
					value = parseInt( elem.css( "zIndex" ), 10 );                                                                   // 93
					if ( !isNaN( value ) && value !== 0 ) {                                                                         // 94
						return value;                                                                                                  // 95
					}                                                                                                               // 96
				}                                                                                                                // 97
				elem = elem.parent();                                                                                            // 98
			}                                                                                                                 // 99
		}                                                                                                                  // 100
                                                                                                                     // 101
		return 0;                                                                                                          // 102
	},                                                                                                                  // 103
                                                                                                                     // 104
	uniqueId: function() {                                                                                              // 105
		return this.each(function() {                                                                                      // 106
			if ( !this.id ) {                                                                                                 // 107
				this.id = "ui-id-" + (++uuid);                                                                                   // 108
			}                                                                                                                 // 109
		});                                                                                                                // 110
	},                                                                                                                  // 111
                                                                                                                     // 112
	removeUniqueId: function() {                                                                                        // 113
		return this.each(function() {                                                                                      // 114
			if ( runiqueId.test( this.id ) ) {                                                                                // 115
				$( this ).removeAttr( "id" );                                                                                    // 116
			}                                                                                                                 // 117
		});                                                                                                                // 118
	}                                                                                                                   // 119
});                                                                                                                  // 120
                                                                                                                     // 121
// selectors                                                                                                         // 122
function focusable( element, isTabIndexNotNaN ) {                                                                    // 123
	var map, mapName, img,                                                                                              // 124
		nodeName = element.nodeName.toLowerCase();                                                                         // 125
	if ( "area" === nodeName ) {                                                                                        // 126
		map = element.parentNode;                                                                                          // 127
		mapName = map.name;                                                                                                // 128
		if ( !element.href || !mapName || map.nodeName.toLowerCase() !== "map" ) {                                         // 129
			return false;                                                                                                     // 130
		}                                                                                                                  // 131
		img = $( "img[usemap=#" + mapName + "]" )[0];                                                                      // 132
		return !!img && visible( img );                                                                                    // 133
	}                                                                                                                   // 134
	return ( /input|select|textarea|button|object/.test( nodeName ) ?                                                   // 135
		!element.disabled :                                                                                                // 136
		"a" === nodeName ?                                                                                                 // 137
			element.href || isTabIndexNotNaN :                                                                                // 138
			isTabIndexNotNaN) &&                                                                                              // 139
		// the element and all of its ancestors must be visible                                                            // 140
		visible( element );                                                                                                // 141
}                                                                                                                    // 142
                                                                                                                     // 143
function visible( element ) {                                                                                        // 144
	return $.expr.filters.visible( element ) &&                                                                         // 145
		!$( element ).parents().addBack().filter(function() {                                                              // 146
			return $.css( this, "visibility" ) === "hidden";                                                                  // 147
		}).length;                                                                                                         // 148
}                                                                                                                    // 149
                                                                                                                     // 150
$.extend( $.expr[ ":" ], {                                                                                           // 151
	data: $.expr.createPseudo ?                                                                                         // 152
		$.expr.createPseudo(function( dataName ) {                                                                         // 153
			return function( elem ) {                                                                                         // 154
				return !!$.data( elem, dataName );                                                                               // 155
			};                                                                                                                // 156
		}) :                                                                                                               // 157
		// support: jQuery <1.8                                                                                            // 158
		function( elem, i, match ) {                                                                                       // 159
			return !!$.data( elem, match[ 3 ] );                                                                              // 160
		},                                                                                                                 // 161
                                                                                                                     // 162
	focusable: function( element ) {                                                                                    // 163
		return focusable( element, !isNaN( $.attr( element, "tabindex" ) ) );                                              // 164
	},                                                                                                                  // 165
                                                                                                                     // 166
	tabbable: function( element ) {                                                                                     // 167
		var tabIndex = $.attr( element, "tabindex" ),                                                                      // 168
			isTabIndexNaN = isNaN( tabIndex );                                                                                // 169
		return ( isTabIndexNaN || tabIndex >= 0 ) && focusable( element, !isTabIndexNaN );                                 // 170
	}                                                                                                                   // 171
});                                                                                                                  // 172
                                                                                                                     // 173
// support: jQuery <1.8                                                                                              // 174
if ( !$( "<a>" ).outerWidth( 1 ).jquery ) {                                                                          // 175
	$.each( [ "Width", "Height" ], function( i, name ) {                                                                // 176
		var side = name === "Width" ? [ "Left", "Right" ] : [ "Top", "Bottom" ],                                           // 177
			type = name.toLowerCase(),                                                                                        // 178
			orig = {                                                                                                          // 179
				innerWidth: $.fn.innerWidth,                                                                                     // 180
				innerHeight: $.fn.innerHeight,                                                                                   // 181
				outerWidth: $.fn.outerWidth,                                                                                     // 182
				outerHeight: $.fn.outerHeight                                                                                    // 183
			};                                                                                                                // 184
                                                                                                                     // 185
		function reduce( elem, size, border, margin ) {                                                                    // 186
			$.each( side, function() {                                                                                        // 187
				size -= parseFloat( $.css( elem, "padding" + this ) ) || 0;                                                      // 188
				if ( border ) {                                                                                                  // 189
					size -= parseFloat( $.css( elem, "border" + this + "Width" ) ) || 0;                                            // 190
				}                                                                                                                // 191
				if ( margin ) {                                                                                                  // 192
					size -= parseFloat( $.css( elem, "margin" + this ) ) || 0;                                                      // 193
				}                                                                                                                // 194
			});                                                                                                               // 195
			return size;                                                                                                      // 196
		}                                                                                                                  // 197
                                                                                                                     // 198
		$.fn[ "inner" + name ] = function( size ) {                                                                        // 199
			if ( size === undefined ) {                                                                                       // 200
				return orig[ "inner" + name ].call( this );                                                                      // 201
			}                                                                                                                 // 202
                                                                                                                     // 203
			return this.each(function() {                                                                                     // 204
				$( this ).css( type, reduce( this, size ) + "px" );                                                              // 205
			});                                                                                                               // 206
		};                                                                                                                 // 207
                                                                                                                     // 208
		$.fn[ "outer" + name] = function( size, margin ) {                                                                 // 209
			if ( typeof size !== "number" ) {                                                                                 // 210
				return orig[ "outer" + name ].call( this, size );                                                                // 211
			}                                                                                                                 // 212
                                                                                                                     // 213
			return this.each(function() {                                                                                     // 214
				$( this).css( type, reduce( this, size, true, margin ) + "px" );                                                 // 215
			});                                                                                                               // 216
		};                                                                                                                 // 217
	});                                                                                                                 // 218
}                                                                                                                    // 219
                                                                                                                     // 220
// support: jQuery <1.8                                                                                              // 221
if ( !$.fn.addBack ) {                                                                                               // 222
	$.fn.addBack = function( selector ) {                                                                               // 223
		return this.add( selector == null ?                                                                                // 224
			this.prevObject : this.prevObject.filter( selector )                                                              // 225
		);                                                                                                                 // 226
	};                                                                                                                  // 227
}                                                                                                                    // 228
                                                                                                                     // 229
// support: jQuery 1.6.1, 1.6.2 (http://bugs.jquery.com/ticket/9413)                                                 // 230
if ( $( "<a>" ).data( "a-b", "a" ).removeData( "a-b" ).data( "a-b" ) ) {                                             // 231
	$.fn.removeData = (function( removeData ) {                                                                         // 232
		return function( key ) {                                                                                           // 233
			if ( arguments.length ) {                                                                                         // 234
				return removeData.call( this, $.camelCase( key ) );                                                              // 235
			} else {                                                                                                          // 236
				return removeData.call( this );                                                                                  // 237
			}                                                                                                                 // 238
		};                                                                                                                 // 239
	})( $.fn.removeData );                                                                                              // 240
}                                                                                                                    // 241
                                                                                                                     // 242
                                                                                                                     // 243
                                                                                                                     // 244
                                                                                                                     // 245
                                                                                                                     // 246
// deprecated                                                                                                        // 247
$.ui.ie = !!/msie [\w.]+/.exec( navigator.userAgent.toLowerCase() );                                                 // 248
                                                                                                                     // 249
$.support.selectstart = "onselectstart" in document.createElement( "div" );                                          // 250
$.fn.extend({                                                                                                        // 251
	disableSelection: function() {                                                                                      // 252
		return this.bind( ( $.support.selectstart ? "selectstart" : "mousedown" ) +                                        // 253
			".ui-disableSelection", function( event ) {                                                                       // 254
				event.preventDefault();                                                                                          // 255
			});                                                                                                               // 256
	},                                                                                                                  // 257
                                                                                                                     // 258
	enableSelection: function() {                                                                                       // 259
		return this.unbind( ".ui-disableSelection" );                                                                      // 260
	}                                                                                                                   // 261
});                                                                                                                  // 262
                                                                                                                     // 263
$.extend( $.ui, {                                                                                                    // 264
	// $.ui.plugin is deprecated. Use $.widget() extensions instead.                                                    // 265
	plugin: {                                                                                                           // 266
		add: function( module, option, set ) {                                                                             // 267
			var i,                                                                                                            // 268
				proto = $.ui[ module ].prototype;                                                                                // 269
			for ( i in set ) {                                                                                                // 270
				proto.plugins[ i ] = proto.plugins[ i ] || [];                                                                   // 271
				proto.plugins[ i ].push( [ option, set[ i ] ] );                                                                 // 272
			}                                                                                                                 // 273
		},                                                                                                                 // 274
		call: function( instance, name, args ) {                                                                           // 275
			var i,                                                                                                            // 276
				set = instance.plugins[ name ];                                                                                  // 277
			if ( !set || !instance.element[ 0 ].parentNode || instance.element[ 0 ].parentNode.nodeType === 11 ) {            // 278
				return;                                                                                                          // 279
			}                                                                                                                 // 280
                                                                                                                     // 281
			for ( i = 0; i < set.length; i++ ) {                                                                              // 282
				if ( instance.options[ set[ i ][ 0 ] ] ) {                                                                       // 283
					set[ i ][ 1 ].apply( instance.element, args );                                                                  // 284
				}                                                                                                                // 285
			}                                                                                                                 // 286
		}                                                                                                                  // 287
	},                                                                                                                  // 288
                                                                                                                     // 289
	// only used by resizable                                                                                           // 290
	hasScroll: function( el, a ) {                                                                                      // 291
                                                                                                                     // 292
		//If overflow is hidden, the element might have extra content, but the user wants to hide it                       // 293
		if ( $( el ).css( "overflow" ) === "hidden") {                                                                     // 294
			return false;                                                                                                     // 295
		}                                                                                                                  // 296
                                                                                                                     // 297
		var scroll = ( a && a === "left" ) ? "scrollLeft" : "scrollTop",                                                   // 298
			has = false;                                                                                                      // 299
                                                                                                                     // 300
		if ( el[ scroll ] > 0 ) {                                                                                          // 301
			return true;                                                                                                      // 302
		}                                                                                                                  // 303
                                                                                                                     // 304
		// TODO: determine which cases actually cause this to happen                                                       // 305
		// if the element doesn't have the scroll set, see if it's possible to                                             // 306
		// set the scroll                                                                                                  // 307
		el[ scroll ] = 1;                                                                                                  // 308
		has = ( el[ scroll ] > 0 );                                                                                        // 309
		el[ scroll ] = 0;                                                                                                  // 310
		return has;                                                                                                        // 311
	}                                                                                                                   // 312
});                                                                                                                  // 313
                                                                                                                     // 314
})( jQuery );                                                                                                        // 315
(function( $, undefined ) {                                                                                          // 316
                                                                                                                     // 317
var uuid = 0,                                                                                                        // 318
	slice = Array.prototype.slice,                                                                                      // 319
	_cleanData = $.cleanData;                                                                                           // 320
$.cleanData = function( elems ) {                                                                                    // 321
	for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {                                                           // 322
		try {                                                                                                              // 323
			$( elem ).triggerHandler( "remove" );                                                                             // 324
		// http://bugs.jquery.com/ticket/8235                                                                              // 325
		} catch( e ) {}                                                                                                    // 326
	}                                                                                                                   // 327
	_cleanData( elems );                                                                                                // 328
};                                                                                                                   // 329
                                                                                                                     // 330
$.widget = function( name, base, prototype ) {                                                                       // 331
	var fullName, existingConstructor, constructor, basePrototype,                                                      // 332
		// proxiedPrototype allows the provided prototype to remain unmodified                                             // 333
		// so that it can be used as a mixin for multiple widgets (#8876)                                                  // 334
		proxiedPrototype = {},                                                                                             // 335
		namespace = name.split( "." )[ 0 ];                                                                                // 336
                                                                                                                     // 337
	name = name.split( "." )[ 1 ];                                                                                      // 338
	fullName = namespace + "-" + name;                                                                                  // 339
                                                                                                                     // 340
	if ( !prototype ) {                                                                                                 // 341
		prototype = base;                                                                                                  // 342
		base = $.Widget;                                                                                                   // 343
	}                                                                                                                   // 344
                                                                                                                     // 345
	// create selector for plugin                                                                                       // 346
	$.expr[ ":" ][ fullName.toLowerCase() ] = function( elem ) {                                                        // 347
		return !!$.data( elem, fullName );                                                                                 // 348
	};                                                                                                                  // 349
                                                                                                                     // 350
	$[ namespace ] = $[ namespace ] || {};                                                                              // 351
	existingConstructor = $[ namespace ][ name ];                                                                       // 352
	constructor = $[ namespace ][ name ] = function( options, element ) {                                               // 353
		// allow instantiation without "new" keyword                                                                       // 354
		if ( !this._createWidget ) {                                                                                       // 355
			return new constructor( options, element );                                                                       // 356
		}                                                                                                                  // 357
                                                                                                                     // 358
		// allow instantiation without initializing for simple inheritance                                                 // 359
		// must use "new" keyword (the code above always passes args)                                                      // 360
		if ( arguments.length ) {                                                                                          // 361
			this._createWidget( options, element );                                                                           // 362
		}                                                                                                                  // 363
	};                                                                                                                  // 364
	// extend with the existing constructor to carry over any static properties                                         // 365
	$.extend( constructor, existingConstructor, {                                                                       // 366
		version: prototype.version,                                                                                        // 367
		// copy the object used to create the prototype in case we need to                                                 // 368
		// redefine the widget later                                                                                       // 369
		_proto: $.extend( {}, prototype ),                                                                                 // 370
		// track widgets that inherit from this widget in case this widget is                                              // 371
		// redefined after a widget inherits from it                                                                       // 372
		_childConstructors: []                                                                                             // 373
	});                                                                                                                 // 374
                                                                                                                     // 375
	basePrototype = new base();                                                                                         // 376
	// we need to make the options hash a property directly on the new instance                                         // 377
	// otherwise we'll modify the options hash on the prototype that we're                                              // 378
	// inheriting from                                                                                                  // 379
	basePrototype.options = $.widget.extend( {}, basePrototype.options );                                               // 380
	$.each( prototype, function( prop, value ) {                                                                        // 381
		if ( !$.isFunction( value ) ) {                                                                                    // 382
			proxiedPrototype[ prop ] = value;                                                                                 // 383
			return;                                                                                                           // 384
		}                                                                                                                  // 385
		proxiedPrototype[ prop ] = (function() {                                                                           // 386
			var _super = function() {                                                                                         // 387
					return base.prototype[ prop ].apply( this, arguments );                                                         // 388
				},                                                                                                               // 389
				_superApply = function( args ) {                                                                                 // 390
					return base.prototype[ prop ].apply( this, args );                                                              // 391
				};                                                                                                               // 392
			return function() {                                                                                               // 393
				var __super = this._super,                                                                                       // 394
					__superApply = this._superApply,                                                                                // 395
					returnValue;                                                                                                    // 396
                                                                                                                     // 397
				this._super = _super;                                                                                            // 398
				this._superApply = _superApply;                                                                                  // 399
                                                                                                                     // 400
				returnValue = value.apply( this, arguments );                                                                    // 401
                                                                                                                     // 402
				this._super = __super;                                                                                           // 403
				this._superApply = __superApply;                                                                                 // 404
                                                                                                                     // 405
				return returnValue;                                                                                              // 406
			};                                                                                                                // 407
		})();                                                                                                              // 408
	});                                                                                                                 // 409
	constructor.prototype = $.widget.extend( basePrototype, {                                                           // 410
		// TODO: remove support for widgetEventPrefix                                                                      // 411
		// always use the name + a colon as the prefix, e.g., draggable:start                                              // 412
		// don't prefix for widgets that aren't DOM-based                                                                  // 413
		widgetEventPrefix: existingConstructor ? basePrototype.widgetEventPrefix : name                                    // 414
	}, proxiedPrototype, {                                                                                              // 415
		constructor: constructor,                                                                                          // 416
		namespace: namespace,                                                                                              // 417
		widgetName: name,                                                                                                  // 418
		widgetFullName: fullName                                                                                           // 419
	});                                                                                                                 // 420
                                                                                                                     // 421
	// If this widget is being redefined then we need to find all widgets that                                          // 422
	// are inheriting from it and redefine all of them so that they inherit from                                        // 423
	// the new version of this widget. We're essentially trying to replace one                                          // 424
	// level in the prototype chain.                                                                                    // 425
	if ( existingConstructor ) {                                                                                        // 426
		$.each( existingConstructor._childConstructors, function( i, child ) {                                             // 427
			var childPrototype = child.prototype;                                                                             // 428
                                                                                                                     // 429
			// redefine the child widget using the same prototype that was                                                    // 430
			// originally used, but inherit from the new version of the base                                                  // 431
			$.widget( childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto );                // 432
		});                                                                                                                // 433
		// remove the list of existing child constructors from the old constructor                                         // 434
		// so the old child constructors can be garbage collected                                                          // 435
		delete existingConstructor._childConstructors;                                                                     // 436
	} else {                                                                                                            // 437
		base._childConstructors.push( constructor );                                                                       // 438
	}                                                                                                                   // 439
                                                                                                                     // 440
	$.widget.bridge( name, constructor );                                                                               // 441
};                                                                                                                   // 442
                                                                                                                     // 443
$.widget.extend = function( target ) {                                                                               // 444
	var input = slice.call( arguments, 1 ),                                                                             // 445
		inputIndex = 0,                                                                                                    // 446
		inputLength = input.length,                                                                                        // 447
		key,                                                                                                               // 448
		value;                                                                                                             // 449
	for ( ; inputIndex < inputLength; inputIndex++ ) {                                                                  // 450
		for ( key in input[ inputIndex ] ) {                                                                               // 451
			value = input[ inputIndex ][ key ];                                                                               // 452
			if ( input[ inputIndex ].hasOwnProperty( key ) && value !== undefined ) {                                         // 453
				// Clone objects                                                                                                 // 454
				if ( $.isPlainObject( value ) ) {                                                                                // 455
					target[ key ] = $.isPlainObject( target[ key ] ) ?                                                              // 456
						$.widget.extend( {}, target[ key ], value ) :                                                                  // 457
						// Don't extend strings, arrays, etc. with objects                                                             // 458
						$.widget.extend( {}, value );                                                                                  // 459
				// Copy everything else by reference                                                                             // 460
				} else {                                                                                                         // 461
					target[ key ] = value;                                                                                          // 462
				}                                                                                                                // 463
			}                                                                                                                 // 464
		}                                                                                                                  // 465
	}                                                                                                                   // 466
	return target;                                                                                                      // 467
};                                                                                                                   // 468
                                                                                                                     // 469
$.widget.bridge = function( name, object ) {                                                                         // 470
	var fullName = object.prototype.widgetFullName || name;                                                             // 471
	$.fn[ name ] = function( options ) {                                                                                // 472
		var isMethodCall = typeof options === "string",                                                                    // 473
			args = slice.call( arguments, 1 ),                                                                                // 474
			returnValue = this;                                                                                               // 475
                                                                                                                     // 476
		// allow multiple hashes to be passed on init                                                                      // 477
		options = !isMethodCall && args.length ?                                                                           // 478
			$.widget.extend.apply( null, [ options ].concat(args) ) :                                                         // 479
			options;                                                                                                          // 480
                                                                                                                     // 481
		if ( isMethodCall ) {                                                                                              // 482
			this.each(function() {                                                                                            // 483
				var methodValue,                                                                                                 // 484
					instance = $.data( this, fullName );                                                                            // 485
				if ( !instance ) {                                                                                               // 486
					return $.error( "cannot call methods on " + name + " prior to initialization; " +                               // 487
						"attempted to call method '" + options + "'" );                                                                // 488
				}                                                                                                                // 489
				if ( !$.isFunction( instance[options] ) || options.charAt( 0 ) === "_" ) {                                       // 490
					return $.error( "no such method '" + options + "' for " + name + " widget instance" );                          // 491
				}                                                                                                                // 492
				methodValue = instance[ options ].apply( instance, args );                                                       // 493
				if ( methodValue !== instance && methodValue !== undefined ) {                                                   // 494
					returnValue = methodValue && methodValue.jquery ?                                                               // 495
						returnValue.pushStack( methodValue.get() ) :                                                                   // 496
						methodValue;                                                                                                   // 497
					return false;                                                                                                   // 498
				}                                                                                                                // 499
			});                                                                                                               // 500
		} else {                                                                                                           // 501
			this.each(function() {                                                                                            // 502
				var instance = $.data( this, fullName );                                                                         // 503
				if ( instance ) {                                                                                                // 504
					instance.option( options || {} )._init();                                                                       // 505
				} else {                                                                                                         // 506
					$.data( this, fullName, new object( options, this ) );                                                          // 507
				}                                                                                                                // 508
			});                                                                                                               // 509
		}                                                                                                                  // 510
                                                                                                                     // 511
		return returnValue;                                                                                                // 512
	};                                                                                                                  // 513
};                                                                                                                   // 514
                                                                                                                     // 515
$.Widget = function( /* options, element */ ) {};                                                                    // 516
$.Widget._childConstructors = [];                                                                                    // 517
                                                                                                                     // 518
$.Widget.prototype = {                                                                                               // 519
	widgetName: "widget",                                                                                               // 520
	widgetEventPrefix: "",                                                                                              // 521
	defaultElement: "<div>",                                                                                            // 522
	options: {                                                                                                          // 523
		disabled: false,                                                                                                   // 524
                                                                                                                     // 525
		// callbacks                                                                                                       // 526
		create: null                                                                                                       // 527
	},                                                                                                                  // 528
	_createWidget: function( options, element ) {                                                                       // 529
		element = $( element || this.defaultElement || this )[ 0 ];                                                        // 530
		this.element = $( element );                                                                                       // 531
		this.uuid = uuid++;                                                                                                // 532
		this.eventNamespace = "." + this.widgetName + this.uuid;                                                           // 533
		this.options = $.widget.extend( {},                                                                                // 534
			this.options,                                                                                                     // 535
			this._getCreateOptions(),                                                                                         // 536
			options );                                                                                                        // 537
                                                                                                                     // 538
		this.bindings = $();                                                                                               // 539
		this.hoverable = $();                                                                                              // 540
		this.focusable = $();                                                                                              // 541
                                                                                                                     // 542
		if ( element !== this ) {                                                                                          // 543
			$.data( element, this.widgetFullName, this );                                                                     // 544
			this._on( true, this.element, {                                                                                   // 545
				remove: function( event ) {                                                                                      // 546
					if ( event.target === element ) {                                                                               // 547
						this.destroy();                                                                                                // 548
					}                                                                                                               // 549
				}                                                                                                                // 550
			});                                                                                                               // 551
			this.document = $( element.style ?                                                                                // 552
				// element within the document                                                                                   // 553
				element.ownerDocument :                                                                                          // 554
				// element is window or document                                                                                 // 555
				element.document || element );                                                                                   // 556
			this.window = $( this.document[0].defaultView || this.document[0].parentWindow );                                 // 557
		}                                                                                                                  // 558
                                                                                                                     // 559
		this._create();                                                                                                    // 560
		this._trigger( "create", null, this._getCreateEventData() );                                                       // 561
		this._init();                                                                                                      // 562
	},                                                                                                                  // 563
	_getCreateOptions: $.noop,                                                                                          // 564
	_getCreateEventData: $.noop,                                                                                        // 565
	_create: $.noop,                                                                                                    // 566
	_init: $.noop,                                                                                                      // 567
                                                                                                                     // 568
	destroy: function() {                                                                                               // 569
		this._destroy();                                                                                                   // 570
		// we can probably remove the unbind calls in 2.0                                                                  // 571
		// all event bindings should go through this._on()                                                                 // 572
		this.element                                                                                                       // 573
			.unbind( this.eventNamespace )                                                                                    // 574
			// 1.9 BC for #7810                                                                                               // 575
			// TODO remove dual storage                                                                                       // 576
			.removeData( this.widgetName )                                                                                    // 577
			.removeData( this.widgetFullName )                                                                                // 578
			// support: jquery <1.6.3                                                                                         // 579
			// http://bugs.jquery.com/ticket/9413                                                                             // 580
			.removeData( $.camelCase( this.widgetFullName ) );                                                                // 581
		this.widget()                                                                                                      // 582
			.unbind( this.eventNamespace )                                                                                    // 583
			.removeAttr( "aria-disabled" )                                                                                    // 584
			.removeClass(                                                                                                     // 585
				this.widgetFullName + "-disabled " +                                                                             // 586
				"ui-state-disabled" );                                                                                           // 587
                                                                                                                     // 588
		// clean up events and states                                                                                      // 589
		this.bindings.unbind( this.eventNamespace );                                                                       // 590
		this.hoverable.removeClass( "ui-state-hover" );                                                                    // 591
		this.focusable.removeClass( "ui-state-focus" );                                                                    // 592
	},                                                                                                                  // 593
	_destroy: $.noop,                                                                                                   // 594
                                                                                                                     // 595
	widget: function() {                                                                                                // 596
		return this.element;                                                                                               // 597
	},                                                                                                                  // 598
                                                                                                                     // 599
	option: function( key, value ) {                                                                                    // 600
		var options = key,                                                                                                 // 601
			parts,                                                                                                            // 602
			curOption,                                                                                                        // 603
			i;                                                                                                                // 604
                                                                                                                     // 605
		if ( arguments.length === 0 ) {                                                                                    // 606
			// don't return a reference to the internal hash                                                                  // 607
			return $.widget.extend( {}, this.options );                                                                       // 608
		}                                                                                                                  // 609
                                                                                                                     // 610
		if ( typeof key === "string" ) {                                                                                   // 611
			// handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }                                                   // 612
			options = {};                                                                                                     // 613
			parts = key.split( "." );                                                                                         // 614
			key = parts.shift();                                                                                              // 615
			if ( parts.length ) {                                                                                             // 616
				curOption = options[ key ] = $.widget.extend( {}, this.options[ key ] );                                         // 617
				for ( i = 0; i < parts.length - 1; i++ ) {                                                                       // 618
					curOption[ parts[ i ] ] = curOption[ parts[ i ] ] || {};                                                        // 619
					curOption = curOption[ parts[ i ] ];                                                                            // 620
				}                                                                                                                // 621
				key = parts.pop();                                                                                               // 622
				if ( value === undefined ) {                                                                                     // 623
					return curOption[ key ] === undefined ? null : curOption[ key ];                                                // 624
				}                                                                                                                // 625
				curOption[ key ] = value;                                                                                        // 626
			} else {                                                                                                          // 627
				if ( value === undefined ) {                                                                                     // 628
					return this.options[ key ] === undefined ? null : this.options[ key ];                                          // 629
				}                                                                                                                // 630
				options[ key ] = value;                                                                                          // 631
			}                                                                                                                 // 632
		}                                                                                                                  // 633
                                                                                                                     // 634
		this._setOptions( options );                                                                                       // 635
                                                                                                                     // 636
		return this;                                                                                                       // 637
	},                                                                                                                  // 638
	_setOptions: function( options ) {                                                                                  // 639
		var key;                                                                                                           // 640
                                                                                                                     // 641
		for ( key in options ) {                                                                                           // 642
			this._setOption( key, options[ key ] );                                                                           // 643
		}                                                                                                                  // 644
                                                                                                                     // 645
		return this;                                                                                                       // 646
	},                                                                                                                  // 647
	_setOption: function( key, value ) {                                                                                // 648
		this.options[ key ] = value;                                                                                       // 649
                                                                                                                     // 650
		if ( key === "disabled" ) {                                                                                        // 651
			this.widget()                                                                                                     // 652
				.toggleClass( this.widgetFullName + "-disabled ui-state-disabled", !!value )                                     // 653
				.attr( "aria-disabled", value );                                                                                 // 654
			this.hoverable.removeClass( "ui-state-hover" );                                                                   // 655
			this.focusable.removeClass( "ui-state-focus" );                                                                   // 656
		}                                                                                                                  // 657
                                                                                                                     // 658
		return this;                                                                                                       // 659
	},                                                                                                                  // 660
                                                                                                                     // 661
	enable: function() {                                                                                                // 662
		return this._setOption( "disabled", false );                                                                       // 663
	},                                                                                                                  // 664
	disable: function() {                                                                                               // 665
		return this._setOption( "disabled", true );                                                                        // 666
	},                                                                                                                  // 667
                                                                                                                     // 668
	_on: function( suppressDisabledCheck, element, handlers ) {                                                         // 669
		var delegateElement,                                                                                               // 670
			instance = this;                                                                                                  // 671
                                                                                                                     // 672
		// no suppressDisabledCheck flag, shuffle arguments                                                                // 673
		if ( typeof suppressDisabledCheck !== "boolean" ) {                                                                // 674
			handlers = element;                                                                                               // 675
			element = suppressDisabledCheck;                                                                                  // 676
			suppressDisabledCheck = false;                                                                                    // 677
		}                                                                                                                  // 678
                                                                                                                     // 679
		// no element argument, shuffle and use this.element                                                               // 680
		if ( !handlers ) {                                                                                                 // 681
			handlers = element;                                                                                               // 682
			element = this.element;                                                                                           // 683
			delegateElement = this.widget();                                                                                  // 684
		} else {                                                                                                           // 685
			// accept selectors, DOM elements                                                                                 // 686
			element = delegateElement = $( element );                                                                         // 687
			this.bindings = this.bindings.add( element );                                                                     // 688
		}                                                                                                                  // 689
                                                                                                                     // 690
		$.each( handlers, function( event, handler ) {                                                                     // 691
			function handlerProxy() {                                                                                         // 692
				// allow widgets to customize the disabled handling                                                              // 693
				// - disabled as an array instead of boolean                                                                     // 694
				// - disabled class as method for disabling individual parts                                                     // 695
				if ( !suppressDisabledCheck &&                                                                                   // 696
						( instance.options.disabled === true ||                                                                        // 697
							$( this ).hasClass( "ui-state-disabled" ) ) ) {                                                               // 698
					return;                                                                                                         // 699
				}                                                                                                                // 700
				return ( typeof handler === "string" ? instance[ handler ] : handler )                                           // 701
					.apply( instance, arguments );                                                                                  // 702
			}                                                                                                                 // 703
                                                                                                                     // 704
			// copy the guid so direct unbinding works                                                                        // 705
			if ( typeof handler !== "string" ) {                                                                              // 706
				handlerProxy.guid = handler.guid =                                                                               // 707
					handler.guid || handlerProxy.guid || $.guid++;                                                                  // 708
			}                                                                                                                 // 709
                                                                                                                     // 710
			var match = event.match( /^(\w+)\s*(.*)$/ ),                                                                      // 711
				eventName = match[1] + instance.eventNamespace,                                                                  // 712
				selector = match[2];                                                                                             // 713
			if ( selector ) {                                                                                                 // 714
				delegateElement.delegate( selector, eventName, handlerProxy );                                                   // 715
			} else {                                                                                                          // 716
				element.bind( eventName, handlerProxy );                                                                         // 717
			}                                                                                                                 // 718
		});                                                                                                                // 719
	},                                                                                                                  // 720
                                                                                                                     // 721
	_off: function( element, eventName ) {                                                                              // 722
		eventName = (eventName || "").split( " " ).join( this.eventNamespace + " " ) + this.eventNamespace;                // 723
		element.unbind( eventName ).undelegate( eventName );                                                               // 724
	},                                                                                                                  // 725
                                                                                                                     // 726
	_delay: function( handler, delay ) {                                                                                // 727
		function handlerProxy() {                                                                                          // 728
			return ( typeof handler === "string" ? instance[ handler ] : handler )                                            // 729
				.apply( instance, arguments );                                                                                   // 730
		}                                                                                                                  // 731
		var instance = this;                                                                                               // 732
		return setTimeout( handlerProxy, delay || 0 );                                                                     // 733
	},                                                                                                                  // 734
                                                                                                                     // 735
	_hoverable: function( element ) {                                                                                   // 736
		this.hoverable = this.hoverable.add( element );                                                                    // 737
		this._on( element, {                                                                                               // 738
			mouseenter: function( event ) {                                                                                   // 739
				$( event.currentTarget ).addClass( "ui-state-hover" );                                                           // 740
			},                                                                                                                // 741
			mouseleave: function( event ) {                                                                                   // 742
				$( event.currentTarget ).removeClass( "ui-state-hover" );                                                        // 743
			}                                                                                                                 // 744
		});                                                                                                                // 745
	},                                                                                                                  // 746
                                                                                                                     // 747
	_focusable: function( element ) {                                                                                   // 748
		this.focusable = this.focusable.add( element );                                                                    // 749
		this._on( element, {                                                                                               // 750
			focusin: function( event ) {                                                                                      // 751
				$( event.currentTarget ).addClass( "ui-state-focus" );                                                           // 752
			},                                                                                                                // 753
			focusout: function( event ) {                                                                                     // 754
				$( event.currentTarget ).removeClass( "ui-state-focus" );                                                        // 755
			}                                                                                                                 // 756
		});                                                                                                                // 757
	},                                                                                                                  // 758
                                                                                                                     // 759
	_trigger: function( type, event, data ) {                                                                           // 760
		var prop, orig,                                                                                                    // 761
			callback = this.options[ type ];                                                                                  // 762
                                                                                                                     // 763
		data = data || {};                                                                                                 // 764
		event = $.Event( event );                                                                                          // 765
		event.type = ( type === this.widgetEventPrefix ?                                                                   // 766
			type :                                                                                                            // 767
			this.widgetEventPrefix + type ).toLowerCase();                                                                    // 768
		// the original event may come from any element                                                                    // 769
		// so we need to reset the target on the new event                                                                 // 770
		event.target = this.element[ 0 ];                                                                                  // 771
                                                                                                                     // 772
		// copy original event properties over to the new event                                                            // 773
		orig = event.originalEvent;                                                                                        // 774
		if ( orig ) {                                                                                                      // 775
			for ( prop in orig ) {                                                                                            // 776
				if ( !( prop in event ) ) {                                                                                      // 777
					event[ prop ] = orig[ prop ];                                                                                   // 778
				}                                                                                                                // 779
			}                                                                                                                 // 780
		}                                                                                                                  // 781
                                                                                                                     // 782
		this.element.trigger( event, data );                                                                               // 783
		return !( $.isFunction( callback ) &&                                                                              // 784
			callback.apply( this.element[0], [ event ].concat( data ) ) === false ||                                          // 785
			event.isDefaultPrevented() );                                                                                     // 786
	}                                                                                                                   // 787
};                                                                                                                   // 788
                                                                                                                     // 789
$.each( { show: "fadeIn", hide: "fadeOut" }, function( method, defaultEffect ) {                                     // 790
	$.Widget.prototype[ "_" + method ] = function( element, options, callback ) {                                       // 791
		if ( typeof options === "string" ) {                                                                               // 792
			options = { effect: options };                                                                                    // 793
		}                                                                                                                  // 794
		var hasOptions,                                                                                                    // 795
			effectName = !options ?                                                                                           // 796
				method :                                                                                                         // 797
				options === true || typeof options === "number" ?                                                                // 798
					defaultEffect :                                                                                                 // 799
					options.effect || defaultEffect;                                                                                // 800
		options = options || {};                                                                                           // 801
		if ( typeof options === "number" ) {                                                                               // 802
			options = { duration: options };                                                                                  // 803
		}                                                                                                                  // 804
		hasOptions = !$.isEmptyObject( options );                                                                          // 805
		options.complete = callback;                                                                                       // 806
		if ( options.delay ) {                                                                                             // 807
			element.delay( options.delay );                                                                                   // 808
		}                                                                                                                  // 809
		if ( hasOptions && $.effects && $.effects.effect[ effectName ] ) {                                                 // 810
			element[ method ]( options );                                                                                     // 811
		} else if ( effectName !== method && element[ effectName ] ) {                                                     // 812
			element[ effectName ]( options.duration, options.easing, callback );                                              // 813
		} else {                                                                                                           // 814
			element.queue(function( next ) {                                                                                  // 815
				$( this )[ method ]();                                                                                           // 816
				if ( callback ) {                                                                                                // 817
					callback.call( element[ 0 ] );                                                                                  // 818
				}                                                                                                                // 819
				next();                                                                                                          // 820
			});                                                                                                               // 821
		}                                                                                                                  // 822
	};                                                                                                                  // 823
});                                                                                                                  // 824
                                                                                                                     // 825
})( jQuery );                                                                                                        // 826
(function( $, undefined ) {                                                                                          // 827
                                                                                                                     // 828
var mouseHandled = false;                                                                                            // 829
$( document ).mouseup( function() {                                                                                  // 830
	mouseHandled = false;                                                                                               // 831
});                                                                                                                  // 832
                                                                                                                     // 833
$.widget("ui.mouse", {                                                                                               // 834
	version: "1.10.3",                                                                                                  // 835
	options: {                                                                                                          // 836
		cancel: "input,textarea,button,select,option",                                                                     // 837
		distance: 1,                                                                                                       // 838
		delay: 0                                                                                                           // 839
	},                                                                                                                  // 840
	_mouseInit: function() {                                                                                            // 841
		var that = this;                                                                                                   // 842
                                                                                                                     // 843
		this.element                                                                                                       // 844
			.bind("mousedown."+this.widgetName, function(event) {                                                             // 845
				return that._mouseDown(event);                                                                                   // 846
			})                                                                                                                // 847
			.bind("click."+this.widgetName, function(event) {                                                                 // 848
				if (true === $.data(event.target, that.widgetName + ".preventClickEvent")) {                                     // 849
					$.removeData(event.target, that.widgetName + ".preventClickEvent");                                             // 850
					event.stopImmediatePropagation();                                                                               // 851
					return false;                                                                                                   // 852
				}                                                                                                                // 853
			});                                                                                                               // 854
                                                                                                                     // 855
		this.started = false;                                                                                              // 856
	},                                                                                                                  // 857
                                                                                                                     // 858
	// TODO: make sure destroying one instance of mouse doesn't mess with                                               // 859
	// other instances of mouse                                                                                         // 860
	_mouseDestroy: function() {                                                                                         // 861
		this.element.unbind("."+this.widgetName);                                                                          // 862
		if ( this._mouseMoveDelegate ) {                                                                                   // 863
			$(document)                                                                                                       // 864
				.unbind("mousemove."+this.widgetName, this._mouseMoveDelegate)                                                   // 865
				.unbind("mouseup."+this.widgetName, this._mouseUpDelegate);                                                      // 866
		}                                                                                                                  // 867
	},                                                                                                                  // 868
                                                                                                                     // 869
	_mouseDown: function(event) {                                                                                       // 870
		// don't let more than one widget handle mouseStart                                                                // 871
		if( mouseHandled ) { return; }                                                                                     // 872
                                                                                                                     // 873
		// we may have missed mouseup (out of window)                                                                      // 874
		(this._mouseStarted && this._mouseUp(event));                                                                      // 875
                                                                                                                     // 876
		this._mouseDownEvent = event;                                                                                      // 877
                                                                                                                     // 878
		var that = this,                                                                                                   // 879
			btnIsLeft = (event.which === 1),                                                                                  // 880
			// event.target.nodeName works around a bug in IE 8 with                                                          // 881
			// disabled inputs (#7620)                                                                                        // 882
			elIsCancel = (typeof this.options.cancel === "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false);
		if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {                                                      // 884
			return true;                                                                                                      // 885
		}                                                                                                                  // 886
                                                                                                                     // 887
		this.mouseDelayMet = !this.options.delay;                                                                          // 888
		if (!this.mouseDelayMet) {                                                                                         // 889
			this._mouseDelayTimer = setTimeout(function() {                                                                   // 890
				that.mouseDelayMet = true;                                                                                       // 891
			}, this.options.delay);                                                                                           // 892
		}                                                                                                                  // 893
                                                                                                                     // 894
		if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {                                                 // 895
			this._mouseStarted = (this._mouseStart(event) !== false);                                                         // 896
			if (!this._mouseStarted) {                                                                                        // 897
				event.preventDefault();                                                                                          // 898
				return true;                                                                                                     // 899
			}                                                                                                                 // 900
		}                                                                                                                  // 901
                                                                                                                     // 902
		// Click event may never have fired (Gecko & Opera)                                                                // 903
		if (true === $.data(event.target, this.widgetName + ".preventClickEvent")) {                                       // 904
			$.removeData(event.target, this.widgetName + ".preventClickEvent");                                               // 905
		}                                                                                                                  // 906
                                                                                                                     // 907
		// these delegates are required to keep context                                                                    // 908
		this._mouseMoveDelegate = function(event) {                                                                        // 909
			return that._mouseMove(event);                                                                                    // 910
		};                                                                                                                 // 911
		this._mouseUpDelegate = function(event) {                                                                          // 912
			return that._mouseUp(event);                                                                                      // 913
		};                                                                                                                 // 914
		$(document)                                                                                                        // 915
			.bind("mousemove."+this.widgetName, this._mouseMoveDelegate)                                                      // 916
			.bind("mouseup."+this.widgetName, this._mouseUpDelegate);                                                         // 917
                                                                                                                     // 918
		event.preventDefault();                                                                                            // 919
                                                                                                                     // 920
		mouseHandled = true;                                                                                               // 921
		return true;                                                                                                       // 922
	},                                                                                                                  // 923
                                                                                                                     // 924
	_mouseMove: function(event) {                                                                                       // 925
		// IE mouseup check - mouseup happened when mouse was out of window                                                // 926
		if ($.ui.ie && ( !document.documentMode || document.documentMode < 9 ) && !event.button) {                         // 927
			return this._mouseUp(event);                                                                                      // 928
		}                                                                                                                  // 929
                                                                                                                     // 930
		if (this._mouseStarted) {                                                                                          // 931
			this._mouseDrag(event);                                                                                           // 932
			return event.preventDefault();                                                                                    // 933
		}                                                                                                                  // 934
                                                                                                                     // 935
		if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {                                                 // 936
			this._mouseStarted =                                                                                              // 937
				(this._mouseStart(this._mouseDownEvent, event) !== false);                                                       // 938
			(this._mouseStarted ? this._mouseDrag(event) : this._mouseUp(event));                                             // 939
		}                                                                                                                  // 940
                                                                                                                     // 941
		return !this._mouseStarted;                                                                                        // 942
	},                                                                                                                  // 943
                                                                                                                     // 944
	_mouseUp: function(event) {                                                                                         // 945
		$(document)                                                                                                        // 946
			.unbind("mousemove."+this.widgetName, this._mouseMoveDelegate)                                                    // 947
			.unbind("mouseup."+this.widgetName, this._mouseUpDelegate);                                                       // 948
                                                                                                                     // 949
		if (this._mouseStarted) {                                                                                          // 950
			this._mouseStarted = false;                                                                                       // 951
                                                                                                                     // 952
			if (event.target === this._mouseDownEvent.target) {                                                               // 953
				$.data(event.target, this.widgetName + ".preventClickEvent", true);                                              // 954
			}                                                                                                                 // 955
                                                                                                                     // 956
			this._mouseStop(event);                                                                                           // 957
		}                                                                                                                  // 958
                                                                                                                     // 959
		return false;                                                                                                      // 960
	},                                                                                                                  // 961
                                                                                                                     // 962
	_mouseDistanceMet: function(event) {                                                                                // 963
		return (Math.max(                                                                                                  // 964
				Math.abs(this._mouseDownEvent.pageX - event.pageX),                                                              // 965
				Math.abs(this._mouseDownEvent.pageY - event.pageY)                                                               // 966
			) >= this.options.distance                                                                                        // 967
		);                                                                                                                 // 968
	},                                                                                                                  // 969
                                                                                                                     // 970
	_mouseDelayMet: function(/* event */) {                                                                             // 971
		return this.mouseDelayMet;                                                                                         // 972
	},                                                                                                                  // 973
                                                                                                                     // 974
	// These are placeholder methods, to be overriden by extending plugin                                               // 975
	_mouseStart: function(/* event */) {},                                                                              // 976
	_mouseDrag: function(/* event */) {},                                                                               // 977
	_mouseStop: function(/* event */) {},                                                                               // 978
	_mouseCapture: function(/* event */) { return true; }                                                               // 979
});                                                                                                                  // 980
                                                                                                                     // 981
})(jQuery);                                                                                                          // 982
(function( $, undefined ) {                                                                                          // 983
                                                                                                                     // 984
/*jshint loopfunc: true */                                                                                           // 985
                                                                                                                     // 986
function isOverAxis( x, reference, size ) {                                                                          // 987
	return ( x > reference ) && ( x < ( reference + size ) );                                                           // 988
}                                                                                                                    // 989
                                                                                                                     // 990
function isFloating(item) {                                                                                          // 991
	return (/left|right/).test(item.css("float")) || (/inline|table-cell/).test(item.css("display"));                   // 992
}                                                                                                                    // 993
                                                                                                                     // 994
$.widget("ui.draggable", $.ui.mouse, {                                                                               // 995
	version: "@VERSION",                                                                                                // 996
	widgetEventPrefix: "drag",                                                                                          // 997
	options: {                                                                                                          // 998
		addClasses: true,                                                                                                  // 999
		appendTo: "parent",                                                                                                // 1000
		axis: false,                                                                                                       // 1001
		connectToSortable: false,                                                                                          // 1002
		containment: false,                                                                                                // 1003
		cursor: "auto",                                                                                                    // 1004
		cursorAt: false,                                                                                                   // 1005
		grid: false,                                                                                                       // 1006
		handle: false,                                                                                                     // 1007
		helper: "original",                                                                                                // 1008
		iframeFix: false,                                                                                                  // 1009
		opacity: false,                                                                                                    // 1010
		refreshPositions: false,                                                                                           // 1011
		revert: false,                                                                                                     // 1012
		revertDuration: 500,                                                                                               // 1013
		scope: "default",                                                                                                  // 1014
		scroll: true,                                                                                                      // 1015
		scrollSensitivity: 20,                                                                                             // 1016
		scrollSpeed: 20,                                                                                                   // 1017
		snap: false,                                                                                                       // 1018
		snapMode: "both",                                                                                                  // 1019
		snapTolerance: 20,                                                                                                 // 1020
		stack: false,                                                                                                      // 1021
		zIndex: false,                                                                                                     // 1022
                                                                                                                     // 1023
		// callbacks                                                                                                       // 1024
		drag: null,                                                                                                        // 1025
		start: null,                                                                                                       // 1026
		stop: null                                                                                                         // 1027
	},                                                                                                                  // 1028
	_create: function() {                                                                                               // 1029
                                                                                                                     // 1030
		if (this.options.helper === "original" && !(/^(?:r|a|f)/).test(this.element.css("position"))) {                    // 1031
			this.element[0].style.position = "relative";                                                                      // 1032
		}                                                                                                                  // 1033
		if (this.options.addClasses){                                                                                      // 1034
			this.element.addClass("ui-draggable");                                                                            // 1035
		}                                                                                                                  // 1036
		if (this.options.disabled){                                                                                        // 1037
			this.element.addClass("ui-draggable-disabled");                                                                   // 1038
		}                                                                                                                  // 1039
                                                                                                                     // 1040
		this._mouseInit();                                                                                                 // 1041
                                                                                                                     // 1042
	},                                                                                                                  // 1043
                                                                                                                     // 1044
	_destroy: function() {                                                                                              // 1045
		this.element.removeClass( "ui-draggable ui-draggable-dragging ui-draggable-disabled" );                            // 1046
		this._mouseDestroy();                                                                                              // 1047
	},                                                                                                                  // 1048
                                                                                                                     // 1049
	_mouseCapture: function(event) {                                                                                    // 1050
                                                                                                                     // 1051
		var o = this.options;                                                                                              // 1052
                                                                                                                     // 1053
		// among others, prevent a drag on a resizable-handle                                                              // 1054
		if (this.helper || o.disabled || $(event.target).closest(".ui-resizable-handle").length > 0) {                     // 1055
			return false;                                                                                                     // 1056
		}                                                                                                                  // 1057
                                                                                                                     // 1058
		//Quit if we're not on a valid handle                                                                              // 1059
		this.handle = this._getHandle(event);                                                                              // 1060
		if (!this.handle) {                                                                                                // 1061
			return false;                                                                                                     // 1062
		}                                                                                                                  // 1063
                                                                                                                     // 1064
		$(o.iframeFix === true ? "iframe" : o.iframeFix).each(function() {                                                 // 1065
			$("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>")                                         // 1066
			.css({                                                                                                            // 1067
				width: this.offsetWidth+"px", height: this.offsetHeight+"px",                                                    // 1068
				position: "absolute", opacity: "0.001", zIndex: 1000                                                             // 1069
			})                                                                                                                // 1070
			.css($(this).offset())                                                                                            // 1071
			.appendTo("body");                                                                                                // 1072
		});                                                                                                                // 1073
                                                                                                                     // 1074
		return true;                                                                                                       // 1075
                                                                                                                     // 1076
	},                                                                                                                  // 1077
                                                                                                                     // 1078
	_mouseStart: function(event) {                                                                                      // 1079
                                                                                                                     // 1080
		var o = this.options;                                                                                              // 1081
                                                                                                                     // 1082
		//Create and append the visible helper                                                                             // 1083
		this.helper = this._createHelper(event);                                                                           // 1084
                                                                                                                     // 1085
		this.helper.addClass("ui-draggable-dragging");                                                                     // 1086
                                                                                                                     // 1087
		//Cache the helper size                                                                                            // 1088
		this._cacheHelperProportions();                                                                                    // 1089
                                                                                                                     // 1090
		//If ddmanager is used for droppables, set the global draggable                                                    // 1091
		if($.ui.ddmanager) {                                                                                               // 1092
			$.ui.ddmanager.current = this;                                                                                    // 1093
		}                                                                                                                  // 1094
                                                                                                                     // 1095
		/*                                                                                                                 // 1096
		 * - Position generation -                                                                                         // 1097
		 * This block generates everything position related - it's the core of draggables.                                 // 1098
		 */                                                                                                                // 1099
                                                                                                                     // 1100
		//Cache the margins of the original element                                                                        // 1101
		this._cacheMargins();                                                                                              // 1102
                                                                                                                     // 1103
		//Store the helper's css position                                                                                  // 1104
		this.cssPosition = this.helper.css( "position" );                                                                  // 1105
		this.scrollParent = this.helper.scrollParent();                                                                    // 1106
		this.offsetParent = this.helper.offsetParent();                                                                    // 1107
		this.offsetParentCssPosition = this.offsetParent.css( "position" );                                                // 1108
                                                                                                                     // 1109
		//The element's absolute position on the page minus margins                                                        // 1110
		this.offset = this.positionAbs = this.element.offset();                                                            // 1111
		this.offset = {                                                                                                    // 1112
			top: this.offset.top - this.margins.top,                                                                          // 1113
			left: this.offset.left - this.margins.left                                                                        // 1114
		};                                                                                                                 // 1115
                                                                                                                     // 1116
		//Reset scroll cache                                                                                               // 1117
		this.offset.scroll = false;                                                                                        // 1118
                                                                                                                     // 1119
		$.extend(this.offset, {                                                                                            // 1120
			click: { //Where the click happened, relative to the element                                                      // 1121
				left: event.pageX - this.offset.left,                                                                            // 1122
				top: event.pageY - this.offset.top                                                                               // 1123
			},                                                                                                                // 1124
			parent: this._getParentOffset(),                                                                                  // 1125
			relative: this._getRelativeOffset() //This is a relative to absolute position minus the actual position calculation - only used for relative positioned helper
		});                                                                                                                // 1127
                                                                                                                     // 1128
		//Generate the original position                                                                                   // 1129
		this.originalPosition = this.position = this._generatePosition(event);                                             // 1130
		this.originalPageX = event.pageX;                                                                                  // 1131
		this.originalPageY = event.pageY;                                                                                  // 1132
                                                                                                                     // 1133
		//Adjust the mouse offset relative to the helper if "cursorAt" is supplied                                         // 1134
		(o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt));                                                          // 1135
                                                                                                                     // 1136
		//Set a containment if given in the options                                                                        // 1137
		this._setContainment();                                                                                            // 1138
                                                                                                                     // 1139
		//Trigger event + callbacks                                                                                        // 1140
		if(this._trigger("start", event) === false) {                                                                      // 1141
			this._clear();                                                                                                    // 1142
			return false;                                                                                                     // 1143
		}                                                                                                                  // 1144
                                                                                                                     // 1145
		//Recache the helper size                                                                                          // 1146
		this._cacheHelperProportions();                                                                                    // 1147
                                                                                                                     // 1148
		//Prepare the droppable offsets                                                                                    // 1149
		if ($.ui.ddmanager && !o.dropBehaviour) {                                                                          // 1150
			$.ui.ddmanager.prepareOffsets(this, event);                                                                       // 1151
		}                                                                                                                  // 1152
                                                                                                                     // 1153
                                                                                                                     // 1154
		this._mouseDrag(event, true); //Execute the drag once - this causes the helper not to be visible before getting its correct position
                                                                                                                     // 1156
		//If the ddmanager is used for droppables, inform the manager that dragging has started (see #5003)                // 1157
		if ( $.ui.ddmanager ) {                                                                                            // 1158
			$.ui.ddmanager.dragStart(this, event);                                                                            // 1159
		}                                                                                                                  // 1160
                                                                                                                     // 1161
		return true;                                                                                                       // 1162
	},                                                                                                                  // 1163
                                                                                                                     // 1164
	_mouseDrag: function(event, noPropagation) {                                                                        // 1165
		// reset any necessary cached properties (see #5009)                                                               // 1166
		if ( this.offsetParentCssPosition === "fixed" ) {                                                                  // 1167
			this.offset.parent = this._getParentOffset();                                                                     // 1168
		}                                                                                                                  // 1169
                                                                                                                     // 1170
		//Compute the helpers position                                                                                     // 1171
		this.position = this._generatePosition(event);                                                                     // 1172
		this.positionAbs = this._convertPositionTo("absolute");                                                            // 1173
                                                                                                                     // 1174
		//Call plugins and callbacks and use the resulting position if something is returned                               // 1175
		if (!noPropagation) {                                                                                              // 1176
			var ui = this._uiHash();                                                                                          // 1177
			if(this._trigger("drag", event, ui) === false) {                                                                  // 1178
				this._mouseUp({});                                                                                               // 1179
				return false;                                                                                                    // 1180
			}                                                                                                                 // 1181
			this.position = ui.position;                                                                                      // 1182
		}                                                                                                                  // 1183
                                                                                                                     // 1184
		if(!this.options.axis || this.options.axis !== "y") {                                                              // 1185
			this.helper[0].style.left = this.position.left+"px";                                                              // 1186
		}                                                                                                                  // 1187
		if(!this.options.axis || this.options.axis !== "x") {                                                              // 1188
			this.helper[0].style.top = this.position.top+"px";                                                                // 1189
		}                                                                                                                  // 1190
		if($.ui.ddmanager) {                                                                                               // 1191
			$.ui.ddmanager.drag(this, event);                                                                                 // 1192
		}                                                                                                                  // 1193
                                                                                                                     // 1194
		return false;                                                                                                      // 1195
	},                                                                                                                  // 1196
                                                                                                                     // 1197
	_mouseStop: function(event) {                                                                                       // 1198
                                                                                                                     // 1199
		//If we are using droppables, inform the manager about the drop                                                    // 1200
		var that = this,                                                                                                   // 1201
			dropped = false;                                                                                                  // 1202
		if ($.ui.ddmanager && !this.options.dropBehaviour) {                                                               // 1203
			dropped = $.ui.ddmanager.drop(this, event);                                                                       // 1204
		}                                                                                                                  // 1205
                                                                                                                     // 1206
		//if a drop comes from outside (a sortable)                                                                        // 1207
		if(this.dropped) {                                                                                                 // 1208
			dropped = this.dropped;                                                                                           // 1209
			this.dropped = false;                                                                                             // 1210
		}                                                                                                                  // 1211
                                                                                                                     // 1212
		//if the original element is no longer in the DOM don't bother to continue (see #8269)                             // 1213
		if ( this.options.helper === "original" && !$.contains( this.element[ 0 ].ownerDocument, this.element[ 0 ] ) ) {   // 1214
			return false;                                                                                                     // 1215
		}                                                                                                                  // 1216
                                                                                                                     // 1217
		if((this.options.revert === "invalid" && !dropped) || (this.options.revert === "valid" && dropped) || this.options.revert === true || ($.isFunction(this.options.revert) && this.options.revert.call(this.element, dropped))) {
			$(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {             // 1219
				if(that._trigger("stop", event) !== false) {                                                                     // 1220
					that._clear();                                                                                                  // 1221
				}                                                                                                                // 1222
			});                                                                                                               // 1223
		} else {                                                                                                           // 1224
			if(this._trigger("stop", event) !== false) {                                                                      // 1225
				this._clear();                                                                                                   // 1226
			}                                                                                                                 // 1227
		}                                                                                                                  // 1228
                                                                                                                     // 1229
		return false;                                                                                                      // 1230
	},                                                                                                                  // 1231
                                                                                                                     // 1232
	_mouseUp: function(event) {                                                                                         // 1233
		//Remove frame helpers                                                                                             // 1234
		$("div.ui-draggable-iframeFix").each(function() {                                                                  // 1235
			this.parentNode.removeChild(this);                                                                                // 1236
		});                                                                                                                // 1237
                                                                                                                     // 1238
		//If the ddmanager is used for droppables, inform the manager that dragging has stopped (see #5003)                // 1239
		if( $.ui.ddmanager ) {                                                                                             // 1240
			$.ui.ddmanager.dragStop(this, event);                                                                             // 1241
		}                                                                                                                  // 1242
                                                                                                                     // 1243
		return $.ui.mouse.prototype._mouseUp.call(this, event);                                                            // 1244
	},                                                                                                                  // 1245
                                                                                                                     // 1246
	cancel: function() {                                                                                                // 1247
                                                                                                                     // 1248
		if(this.helper.is(".ui-draggable-dragging")) {                                                                     // 1249
			this._mouseUp({});                                                                                                // 1250
		} else {                                                                                                           // 1251
			this._clear();                                                                                                    // 1252
		}                                                                                                                  // 1253
                                                                                                                     // 1254
		return this;                                                                                                       // 1255
                                                                                                                     // 1256
	},                                                                                                                  // 1257
                                                                                                                     // 1258
	_getHandle: function(event) {                                                                                       // 1259
		return this.options.handle ?                                                                                       // 1260
			!!$( event.target ).closest( this.element.find( this.options.handle ) ).length :                                  // 1261
			true;                                                                                                             // 1262
	},                                                                                                                  // 1263
                                                                                                                     // 1264
	_createHelper: function(event) {                                                                                    // 1265
                                                                                                                     // 1266
		var o = this.options,                                                                                              // 1267
			helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event])) : (o.helper === "clone" ? this.element.clone().removeAttr("id") : this.element);
                                                                                                                     // 1269
		if(!helper.parents("body").length) {                                                                               // 1270
			helper.appendTo((o.appendTo === "parent" ? this.element[0].parentNode : o.appendTo));                             // 1271
		}                                                                                                                  // 1272
                                                                                                                     // 1273
		if(helper[0] !== this.element[0] && !(/(fixed|absolute)/).test(helper.css("position"))) {                          // 1274
			helper.css("position", "absolute");                                                                               // 1275
		}                                                                                                                  // 1276
                                                                                                                     // 1277
		return helper;                                                                                                     // 1278
                                                                                                                     // 1279
	},                                                                                                                  // 1280
                                                                                                                     // 1281
	_adjustOffsetFromHelper: function(obj) {                                                                            // 1282
		if (typeof obj === "string") {                                                                                     // 1283
			obj = obj.split(" ");                                                                                             // 1284
		}                                                                                                                  // 1285
		if ($.isArray(obj)) {                                                                                              // 1286
			obj = {left: +obj[0], top: +obj[1] || 0};                                                                         // 1287
		}                                                                                                                  // 1288
		if ("left" in obj) {                                                                                               // 1289
			this.offset.click.left = obj.left + this.margins.left;                                                            // 1290
		}                                                                                                                  // 1291
		if ("right" in obj) {                                                                                              // 1292
			this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left;                            // 1293
		}                                                                                                                  // 1294
		if ("top" in obj) {                                                                                                // 1295
			this.offset.click.top = obj.top + this.margins.top;                                                               // 1296
		}                                                                                                                  // 1297
		if ("bottom" in obj) {                                                                                             // 1298
			this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top;                            // 1299
		}                                                                                                                  // 1300
	},                                                                                                                  // 1301
                                                                                                                     // 1302
	_getParentOffset: function() {                                                                                      // 1303
                                                                                                                     // 1304
		//Get the offsetParent and cache its position                                                                      // 1305
		var po = this.offsetParent.offset();                                                                               // 1306
                                                                                                                     // 1307
		// This is a special case where we need to modify a offset calculated on start, since the following happened:      // 1308
		// 1. The position of the helper is absolute, so it's position is calculated based on the next positioned parent   // 1309
		// 2. The actual offset parent is a child of the scroll parent, and the scroll parent isn't the document, which means that
		//    the scroll is included in the initial calculation of the offset of the parent, and never recalculated upon drag
		if(this.cssPosition === "absolute" && this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0])) {
			po.left += this.scrollParent.scrollLeft();                                                                        // 1313
			po.top += this.scrollParent.scrollTop();                                                                          // 1314
		}                                                                                                                  // 1315
                                                                                                                     // 1316
		//This needs to be actually done for all browsers, since pageX/pageY includes this information                     // 1317
		//Ugly IE fix                                                                                                      // 1318
		if((this.offsetParent[0] === document.body) ||                                                                     // 1319
			(this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() === "html" && $.ui.ie)) {             // 1320
			po = { top: 0, left: 0 };                                                                                         // 1321
		}                                                                                                                  // 1322
                                                                                                                     // 1323
		return {                                                                                                           // 1324
			top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"),10) || 0),                                        // 1325
			left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"),10) || 0)                                      // 1326
		};                                                                                                                 // 1327
                                                                                                                     // 1328
	},                                                                                                                  // 1329
                                                                                                                     // 1330
	_getRelativeOffset: function() {                                                                                    // 1331
                                                                                                                     // 1332
		if(this.cssPosition === "relative") {                                                                              // 1333
			var p = this.element.position();                                                                                  // 1334
			return {                                                                                                          // 1335
				top: p.top - (parseInt(this.helper.css("top"),10) || 0) + this.scrollParent.scrollTop(),                         // 1336
				left: p.left - (parseInt(this.helper.css("left"),10) || 0) + this.scrollParent.scrollLeft()                      // 1337
			};                                                                                                                // 1338
		} else {                                                                                                           // 1339
			return { top: 0, left: 0 };                                                                                       // 1340
		}                                                                                                                  // 1341
                                                                                                                     // 1342
	},                                                                                                                  // 1343
                                                                                                                     // 1344
	_cacheMargins: function() {                                                                                         // 1345
		this.margins = {                                                                                                   // 1346
			left: (parseInt(this.element.css("marginLeft"),10) || 0),                                                         // 1347
			top: (parseInt(this.element.css("marginTop"),10) || 0),                                                           // 1348
			right: (parseInt(this.element.css("marginRight"),10) || 0),                                                       // 1349
			bottom: (parseInt(this.element.css("marginBottom"),10) || 0)                                                      // 1350
		};                                                                                                                 // 1351
	},                                                                                                                  // 1352
                                                                                                                     // 1353
	_cacheHelperProportions: function() {                                                                               // 1354
		this.helperProportions = {                                                                                         // 1355
			width: this.helper.outerWidth(),                                                                                  // 1356
			height: this.helper.outerHeight()                                                                                 // 1357
		};                                                                                                                 // 1358
	},                                                                                                                  // 1359
                                                                                                                     // 1360
	_setContainment: function() {                                                                                       // 1361
                                                                                                                     // 1362
		var over, c, ce,                                                                                                   // 1363
			o = this.options;                                                                                                 // 1364
                                                                                                                     // 1365
		if ( !o.containment ) {                                                                                            // 1366
			this.containment = null;                                                                                          // 1367
			return;                                                                                                           // 1368
		}                                                                                                                  // 1369
                                                                                                                     // 1370
		if ( o.containment === "window" ) {                                                                                // 1371
			this.containment = [                                                                                              // 1372
				$( window ).scrollLeft() - this.offset.relative.left - this.offset.parent.left,                                  // 1373
				$( window ).scrollTop() - this.offset.relative.top - this.offset.parent.top,                                     // 1374
				$( window ).scrollLeft() + $( window ).width() - this.helperProportions.width - this.margins.left,               // 1375
				$( window ).scrollTop() + ( $( window ).height() || document.body.parentNode.scrollHeight ) - this.helperProportions.height - this.margins.top
			];                                                                                                                // 1377
			return;                                                                                                           // 1378
		}                                                                                                                  // 1379
                                                                                                                     // 1380
		if ( o.containment === "document") {                                                                               // 1381
			this.containment = [                                                                                              // 1382
				0,                                                                                                               // 1383
				0,                                                                                                               // 1384
				$( document ).width() - this.helperProportions.width - this.margins.left,                                        // 1385
				( $( document ).height() || document.body.parentNode.scrollHeight ) - this.helperProportions.height - this.margins.top
			];                                                                                                                // 1387
			return;                                                                                                           // 1388
		}                                                                                                                  // 1389
                                                                                                                     // 1390
		if ( o.containment.constructor === Array ) {                                                                       // 1391
			this.containment = o.containment;                                                                                 // 1392
			return;                                                                                                           // 1393
		}                                                                                                                  // 1394
                                                                                                                     // 1395
		if ( o.containment === "parent" ) {                                                                                // 1396
			o.containment = this.helper[ 0 ].parentNode;                                                                      // 1397
		}                                                                                                                  // 1398
                                                                                                                     // 1399
		c = $( o.containment );                                                                                            // 1400
		ce = c[ 0 ];                                                                                                       // 1401
                                                                                                                     // 1402
		if( !ce ) {                                                                                                        // 1403
			return;                                                                                                           // 1404
		}                                                                                                                  // 1405
                                                                                                                     // 1406
		over = c.css( "overflow" ) !== "hidden";                                                                           // 1407
                                                                                                                     // 1408
		this.containment = [                                                                                               // 1409
			( parseInt( c.css( "borderLeftWidth" ), 10 ) || 0 ) + ( parseInt( c.css( "paddingLeft" ), 10 ) || 0 ),            // 1410
			( parseInt( c.css( "borderTopWidth" ), 10 ) || 0 ) + ( parseInt( c.css( "paddingTop" ), 10 ) || 0 ) ,             // 1411
			( over ? Math.max( ce.scrollWidth, ce.offsetWidth ) : ce.offsetWidth ) - ( parseInt( c.css( "borderRightWidth" ), 10 ) || 0 ) - ( parseInt( c.css( "paddingRight" ), 10 ) || 0 ) - this.helperProportions.width - this.margins.left - this.margins.right,
			( over ? Math.max( ce.scrollHeight, ce.offsetHeight ) : ce.offsetHeight ) - ( parseInt( c.css( "borderBottomWidth" ), 10 ) || 0 ) - ( parseInt( c.css( "paddingBottom" ), 10 ) || 0 ) - this.helperProportions.height - this.margins.top  - this.margins.bottom
		];                                                                                                                 // 1414
		this.relative_container = c;                                                                                       // 1415
	},                                                                                                                  // 1416
                                                                                                                     // 1417
	_convertPositionTo: function(d, pos) {                                                                              // 1418
                                                                                                                     // 1419
		if(!pos) {                                                                                                         // 1420
			pos = this.position;                                                                                              // 1421
		}                                                                                                                  // 1422
                                                                                                                     // 1423
		var mod = d === "absolute" ? 1 : -1,                                                                               // 1424
			scroll = this.cssPosition === "absolute" && !( this.scrollParent[ 0 ] !== document && $.contains( this.scrollParent[ 0 ], this.offsetParent[ 0 ] ) ) ? this.offsetParent : this.scrollParent;
                                                                                                                     // 1426
		//Cache the scroll                                                                                                 // 1427
		if (!this.offset.scroll) {                                                                                         // 1428
			this.offset.scroll = {top : scroll.scrollTop(), left : scroll.scrollLeft()};                                      // 1429
		}                                                                                                                  // 1430
                                                                                                                     // 1431
		return {                                                                                                           // 1432
			top: (                                                                                                            // 1433
				pos.top	+																// The absolute mouse position                                                          // 1434
				this.offset.relative.top * mod +										// Only for relative positioned nodes: Relative offset from element to offset parent
				this.offset.parent.top * mod -										// The offsetParent's offset without borders (offset + border)           // 1436
				( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : this.offset.scroll.top ) * mod )             // 1437
			),                                                                                                                // 1438
			left: (                                                                                                           // 1439
				pos.left +																// The absolute mouse position                                                         // 1440
				this.offset.relative.left * mod +										// Only for relative positioned nodes: Relative offset from element to offset parent
				this.offset.parent.left * mod	-										// The offsetParent's offset without borders (offset + border)          // 1442
				( ( this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : this.offset.scroll.left ) * mod )           // 1443
			)                                                                                                                 // 1444
		};                                                                                                                 // 1445
                                                                                                                     // 1446
	},                                                                                                                  // 1447
                                                                                                                     // 1448
	_generatePosition: function(event) {                                                                                // 1449
                                                                                                                     // 1450
		var containment, co, top, left,                                                                                    // 1451
			o = this.options,                                                                                                 // 1452
			scroll = this.cssPosition === "absolute" && !( this.scrollParent[ 0 ] !== document && $.contains( this.scrollParent[ 0 ], this.offsetParent[ 0 ] ) ) ? this.offsetParent : this.scrollParent,
			pageX = event.pageX,                                                                                              // 1454
			pageY = event.pageY;                                                                                              // 1455
                                                                                                                     // 1456
		//Cache the scroll                                                                                                 // 1457
		if (!this.offset.scroll) {                                                                                         // 1458
			this.offset.scroll = {top : scroll.scrollTop(), left : scroll.scrollLeft()};                                      // 1459
		}                                                                                                                  // 1460
                                                                                                                     // 1461
		/*                                                                                                                 // 1462
		 * - Position constraining -                                                                                       // 1463
		 * Constrain the position to a mix of grid, containment.                                                           // 1464
		 */                                                                                                                // 1465
                                                                                                                     // 1466
		// If we are not dragging yet, we won't check for options                                                          // 1467
		if ( this.originalPosition ) {                                                                                     // 1468
			if ( this.containment ) {                                                                                         // 1469
				if ( this.relative_container ){                                                                                  // 1470
					co = this.relative_container.offset();                                                                          // 1471
					containment = [                                                                                                 // 1472
						this.containment[ 0 ] + co.left,                                                                               // 1473
						this.containment[ 1 ] + co.top,                                                                                // 1474
						this.containment[ 2 ] + co.left,                                                                               // 1475
						this.containment[ 3 ] + co.top                                                                                 // 1476
					];                                                                                                              // 1477
				}                                                                                                                // 1478
				else {                                                                                                           // 1479
					containment = this.containment;                                                                                 // 1480
				}                                                                                                                // 1481
                                                                                                                     // 1482
				if(event.pageX - this.offset.click.left < containment[0]) {                                                      // 1483
					pageX = containment[0] + this.offset.click.left;                                                                // 1484
				}                                                                                                                // 1485
				if(event.pageY - this.offset.click.top < containment[1]) {                                                       // 1486
					pageY = containment[1] + this.offset.click.top;                                                                 // 1487
				}                                                                                                                // 1488
				if(event.pageX - this.offset.click.left > containment[2]) {                                                      // 1489
					pageX = containment[2] + this.offset.click.left;                                                                // 1490
				}                                                                                                                // 1491
				if(event.pageY - this.offset.click.top > containment[3]) {                                                       // 1492
					pageY = containment[3] + this.offset.click.top;                                                                 // 1493
				}                                                                                                                // 1494
			}                                                                                                                 // 1495
                                                                                                                     // 1496
			if(o.grid) {                                                                                                      // 1497
				//Check for grid elements set to 0 to prevent divide by 0 error causing invalid argument errors in IE (see ticket #6950)
				top = o.grid[1] ? this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY;
				pageY = containment ? ((top - this.offset.click.top >= containment[1] || top - this.offset.click.top > containment[3]) ? top : ((top - this.offset.click.top >= containment[1]) ? top - o.grid[1] : top + o.grid[1])) : top;
                                                                                                                     // 1501
				left = o.grid[0] ? this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX;
				pageX = containment ? ((left - this.offset.click.left >= containment[0] || left - this.offset.click.left > containment[2]) ? left : ((left - this.offset.click.left >= containment[0]) ? left - o.grid[0] : left + o.grid[0])) : left;
			}                                                                                                                 // 1504
                                                                                                                     // 1505
		}                                                                                                                  // 1506
                                                                                                                     // 1507
		return {                                                                                                           // 1508
			top: (                                                                                                            // 1509
				pageY -																	// The absolute mouse position                                                           // 1510
				this.offset.click.top	-												// Click offset (relative to the element)                                     // 1511
				this.offset.relative.top -												// Only for relative positioned nodes: Relative offset from element to offset parent
				this.offset.parent.top +												// The offsetParent's offset without borders (offset + border)               // 1513
				( this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : this.offset.scroll.top )                       // 1514
			),                                                                                                                // 1515
			left: (                                                                                                           // 1516
				pageX -																	// The absolute mouse position                                                           // 1517
				this.offset.click.left -												// Click offset (relative to the element)                                    // 1518
				this.offset.relative.left -												// Only for relative positioned nodes: Relative offset from element to offset parent
				this.offset.parent.left +												// The offsetParent's offset without borders (offset + border)              // 1520
				( this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : this.offset.scroll.left )                     // 1521
			)                                                                                                                 // 1522
		};                                                                                                                 // 1523
                                                                                                                     // 1524
	},                                                                                                                  // 1525
                                                                                                                     // 1526
	_clear: function() {                                                                                                // 1527
		this.helper.removeClass("ui-draggable-dragging");                                                                  // 1528
		if(this.helper[0] !== this.element[0] && !this.cancelHelperRemoval) {                                              // 1529
			this.helper.remove();                                                                                             // 1530
		}                                                                                                                  // 1531
		this.helper = null;                                                                                                // 1532
		this.cancelHelperRemoval = false;                                                                                  // 1533
	},                                                                                                                  // 1534
                                                                                                                     // 1535
	// From now on bulk stuff - mainly helpers                                                                          // 1536
                                                                                                                     // 1537
	_trigger: function(type, event, ui) {                                                                               // 1538
		ui = ui || this._uiHash();                                                                                         // 1539
		$.ui.plugin.call(this, type, [event, ui]);                                                                         // 1540
		//The absolute position has to be recalculated after plugins                                                       // 1541
		if(type === "drag") {                                                                                              // 1542
			this.positionAbs = this._convertPositionTo("absolute");                                                           // 1543
		}                                                                                                                  // 1544
		return $.Widget.prototype._trigger.call(this, type, event, ui);                                                    // 1545
	},                                                                                                                  // 1546
                                                                                                                     // 1547
	plugins: {},                                                                                                        // 1548
                                                                                                                     // 1549
	_uiHash: function() {                                                                                               // 1550
		return {                                                                                                           // 1551
			helper: this.helper,                                                                                              // 1552
			position: this.position,                                                                                          // 1553
			originalPosition: this.originalPosition,                                                                          // 1554
			offset: this.positionAbs                                                                                          // 1555
		};                                                                                                                 // 1556
	}                                                                                                                   // 1557
                                                                                                                     // 1558
});                                                                                                                  // 1559
                                                                                                                     // 1560
$.ui.plugin.add("draggable", "connectToSortable", {                                                                  // 1561
	start: function(event, ui) {                                                                                        // 1562
                                                                                                                     // 1563
		var inst = $(this).data("ui-draggable"), o = inst.options,                                                         // 1564
			uiSortable = $.extend({}, ui, { item: inst.element });                                                            // 1565
		inst.sortables = [];                                                                                               // 1566
		$(o.connectToSortable).each(function() {                                                                           // 1567
			var sortable = $.data(this, "ui-sortable");                                                                       // 1568
			if (sortable && !sortable.options.disabled) {                                                                     // 1569
				inst.sortables.push({                                                                                            // 1570
					instance: sortable,                                                                                             // 1571
					shouldRevert: sortable.options.revert                                                                           // 1572
				});                                                                                                              // 1573
				sortable.refreshPositions();	// Call the sortable's refreshPositions at drag start to refresh the containerCache since the sortable container cache is used in drag and needs to be up to date (this will ensure it's initialised as well as being kept in step with any changes that might have happened on the page).
				sortable._trigger("activate", event, uiSortable);                                                                // 1575
			}                                                                                                                 // 1576
		});                                                                                                                // 1577
                                                                                                                     // 1578
	},                                                                                                                  // 1579
	stop: function(event, ui) {                                                                                         // 1580
                                                                                                                     // 1581
		//If we are still over the sortable, we fake the stop event of the sortable, but also remove helper                // 1582
		var inst = $(this).data("ui-draggable"),                                                                           // 1583
			uiSortable = $.extend({}, ui, { item: inst.element });                                                            // 1584
                                                                                                                     // 1585
		$.each(inst.sortables, function() {                                                                                // 1586
			if(this.instance.isOver) {                                                                                        // 1587
                                                                                                                     // 1588
				this.instance.isOver = 0;                                                                                        // 1589
                                                                                                                     // 1590
				inst.cancelHelperRemoval = true; //Don't remove the helper in the draggable instance                             // 1591
				this.instance.cancelHelperRemoval = false; //Remove it in the sortable instance (so sortable plugins like revert still work)
                                                                                                                     // 1593
				//The sortable revert is supported, and we have to set a temporary dropped variable on the draggable to support revert: "valid/invalid"
				if(this.shouldRevert) {                                                                                          // 1595
					this.instance.options.revert = this.shouldRevert;                                                               // 1596
				}                                                                                                                // 1597
                                                                                                                     // 1598
				//Trigger the stop of the sortable                                                                               // 1599
				this.instance._mouseStop(event);                                                                                 // 1600
                                                                                                                     // 1601
				this.instance.options.helper = this.instance.options._helper;                                                    // 1602
                                                                                                                     // 1603
				//If the helper has been the original item, restore properties in the sortable                                   // 1604
				if(inst.options.helper === "original") {                                                                         // 1605
					this.instance.currentItem.css({ top: "auto", left: "auto" });                                                   // 1606
				}                                                                                                                // 1607
                                                                                                                     // 1608
			} else {                                                                                                          // 1609
				this.instance.cancelHelperRemoval = false; //Remove the helper in the sortable instance                          // 1610
				this.instance._trigger("deactivate", event, uiSortable);                                                         // 1611
			}                                                                                                                 // 1612
                                                                                                                     // 1613
		});                                                                                                                // 1614
                                                                                                                     // 1615
	},                                                                                                                  // 1616
	drag: function(event, ui) {                                                                                         // 1617
                                                                                                                     // 1618
		var inst = $(this).data("ui-draggable"), that = this;                                                              // 1619
                                                                                                                     // 1620
		$.each(inst.sortables, function() {                                                                                // 1621
                                                                                                                     // 1622
			var innermostIntersecting = false,                                                                                // 1623
				thisSortable = this;                                                                                             // 1624
                                                                                                                     // 1625
			//Copy over some variables to allow calling the sortable's native _intersectsWith                                 // 1626
			this.instance.positionAbs = inst.positionAbs;                                                                     // 1627
			this.instance.helperProportions = inst.helperProportions;                                                         // 1628
			this.instance.offset.click = inst.offset.click;                                                                   // 1629
                                                                                                                     // 1630
			if(this.instance._intersectsWith(this.instance.containerCache)) {                                                 // 1631
				innermostIntersecting = true;                                                                                    // 1632
				$.each(inst.sortables, function () {                                                                             // 1633
					this.instance.positionAbs = inst.positionAbs;                                                                   // 1634
					this.instance.helperProportions = inst.helperProportions;                                                       // 1635
					this.instance.offset.click = inst.offset.click;                                                                 // 1636
					if (this !== thisSortable &&                                                                                    // 1637
						this.instance._intersectsWith(this.instance.containerCache) &&                                                 // 1638
						$.contains(thisSortable.instance.element[0], this.instance.element[0])                                         // 1639
					) {                                                                                                             // 1640
						innermostIntersecting = false;                                                                                 // 1641
					}                                                                                                               // 1642
					return innermostIntersecting;                                                                                   // 1643
				});                                                                                                              // 1644
			}                                                                                                                 // 1645
                                                                                                                     // 1646
                                                                                                                     // 1647
			if(innermostIntersecting) {                                                                                       // 1648
				//If it intersects, we use a little isOver variable and set it once, so our move-in stuff gets fired only once   // 1649
				if(!this.instance.isOver) {                                                                                      // 1650
                                                                                                                     // 1651
					this.instance.isOver = 1;                                                                                       // 1652
					//Now we fake the start of dragging for the sortable instance,                                                  // 1653
					//by cloning the list group item, appending it to the sortable and using it as inst.currentItem                 // 1654
					//We can then fire the start event of the sortable with our passed browser event, and our own helper (so it doesn't create a new one)
					this.instance.currentItem = $(that).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", true);
					this.instance.options._helper = this.instance.options.helper; //Store helper option to later restore it         // 1657
					this.instance.options.helper = function() { return ui.helper[0]; };                                             // 1658
                                                                                                                     // 1659
					event.target = this.instance.currentItem[0];                                                                    // 1660
					this.instance._mouseCapture(event, true);                                                                       // 1661
					this.instance._mouseStart(event, true, true);                                                                   // 1662
                                                                                                                     // 1663
					//Because the browser event is way off the new appended portlet, we modify a couple of variables to reflect the changes
					this.instance.offset.click.top = inst.offset.click.top;                                                         // 1665
					this.instance.offset.click.left = inst.offset.click.left;                                                       // 1666
					this.instance.offset.parent.left -= inst.offset.parent.left - this.instance.offset.parent.left;                 // 1667
					this.instance.offset.parent.top -= inst.offset.parent.top - this.instance.offset.parent.top;                    // 1668
                                                                                                                     // 1669
					inst._trigger("toSortable", event);                                                                             // 1670
					inst.dropped = this.instance.element; //draggable revert needs that                                             // 1671
					//hack so receive/update callbacks work (mostly)                                                                // 1672
					inst.currentItem = inst.element;                                                                                // 1673
					this.instance.fromOutside = inst;                                                                               // 1674
                                                                                                                     // 1675
				}                                                                                                                // 1676
                                                                                                                     // 1677
				//Provided we did all the previous steps, we can fire the drag event of the sortable on every draggable drag, when it intersects with the sortable
				if(this.instance.currentItem) {                                                                                  // 1679
					this.instance._mouseDrag(event);                                                                                // 1680
				}                                                                                                                // 1681
                                                                                                                     // 1682
			} else {                                                                                                          // 1683
                                                                                                                     // 1684
				//If it doesn't intersect with the sortable, and it intersected before,                                          // 1685
				//we fake the drag stop of the sortable, but make sure it doesn't remove the helper by using cancelHelperRemoval // 1686
				if(this.instance.isOver) {                                                                                       // 1687
                                                                                                                     // 1688
					this.instance.isOver = 0;                                                                                       // 1689
					this.instance.cancelHelperRemoval = true;                                                                       // 1690
                                                                                                                     // 1691
					//Prevent reverting on this forced stop                                                                         // 1692
					this.instance.options.revert = false;                                                                           // 1693
                                                                                                                     // 1694
					// The out event needs to be triggered independently                                                            // 1695
					this.instance._trigger("out", event, this.instance._uiHash(this.instance));                                     // 1696
                                                                                                                     // 1697
					this.instance._mouseStop(event, true);                                                                          // 1698
					this.instance.options.helper = this.instance.options._helper;                                                   // 1699
                                                                                                                     // 1700
					//Now we remove our currentItem, the list group clone again, and the placeholder, and animate the helper back to it's original size
					this.instance.currentItem.remove();                                                                             // 1702
					if(this.instance.placeholder) {                                                                                 // 1703
						this.instance.placeholder.remove();                                                                            // 1704
					}                                                                                                               // 1705
                                                                                                                     // 1706
					inst._trigger("fromSortable", event);                                                                           // 1707
					inst.dropped = false; //draggable revert needs that                                                             // 1708
				}                                                                                                                // 1709
                                                                                                                     // 1710
			}                                                                                                                 // 1711
                                                                                                                     // 1712
		});                                                                                                                // 1713
                                                                                                                     // 1714
	}                                                                                                                   // 1715
});                                                                                                                  // 1716
                                                                                                                     // 1717
$.ui.plugin.add("draggable", "cursor", {                                                                             // 1718
	start: function() {                                                                                                 // 1719
		var t = $("body"), o = $(this).data("ui-draggable").options;                                                       // 1720
		if (t.css("cursor")) {                                                                                             // 1721
			o._cursor = t.css("cursor");                                                                                      // 1722
		}                                                                                                                  // 1723
		t.css("cursor", o.cursor);                                                                                         // 1724
	},                                                                                                                  // 1725
	stop: function() {                                                                                                  // 1726
		var o = $(this).data("ui-draggable").options;                                                                      // 1727
		if (o._cursor) {                                                                                                   // 1728
			$("body").css("cursor", o._cursor);                                                                               // 1729
		}                                                                                                                  // 1730
	}                                                                                                                   // 1731
});                                                                                                                  // 1732
                                                                                                                     // 1733
$.ui.plugin.add("draggable", "opacity", {                                                                            // 1734
	start: function(event, ui) {                                                                                        // 1735
		var t = $(ui.helper), o = $(this).data("ui-draggable").options;                                                    // 1736
		if(t.css("opacity")) {                                                                                             // 1737
			o._opacity = t.css("opacity");                                                                                    // 1738
		}                                                                                                                  // 1739
		t.css("opacity", o.opacity);                                                                                       // 1740
	},                                                                                                                  // 1741
	stop: function(event, ui) {                                                                                         // 1742
		var o = $(this).data("ui-draggable").options;                                                                      // 1743
		if(o._opacity) {                                                                                                   // 1744
			$(ui.helper).css("opacity", o._opacity);                                                                          // 1745
		}                                                                                                                  // 1746
	}                                                                                                                   // 1747
});                                                                                                                  // 1748
                                                                                                                     // 1749
$.ui.plugin.add("draggable", "scroll", {                                                                             // 1750
	start: function() {                                                                                                 // 1751
		var i = $(this).data("ui-draggable");                                                                              // 1752
		if(i.scrollParent[0] !== document && i.scrollParent[0].tagName !== "HTML") {                                       // 1753
			i.overflowOffset = i.scrollParent.offset();                                                                       // 1754
		}                                                                                                                  // 1755
	},                                                                                                                  // 1756
	drag: function( event ) {                                                                                           // 1757
                                                                                                                     // 1758
		var i = $(this).data("ui-draggable"), o = i.options, scrolled = false;                                             // 1759
                                                                                                                     // 1760
		if(i.scrollParent[0] !== document && i.scrollParent[0].tagName !== "HTML") {                                       // 1761
                                                                                                                     // 1762
			if(!o.axis || o.axis !== "x") {                                                                                   // 1763
				if((i.overflowOffset.top + i.scrollParent[0].offsetHeight) - event.pageY < o.scrollSensitivity) {                // 1764
					i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop + o.scrollSpeed;                           // 1765
				} else if(event.pageY - i.overflowOffset.top < o.scrollSensitivity) {                                            // 1766
					i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop - o.scrollSpeed;                           // 1767
				}                                                                                                                // 1768
			}                                                                                                                 // 1769
                                                                                                                     // 1770
			if(!o.axis || o.axis !== "y") {                                                                                   // 1771
				if((i.overflowOffset.left + i.scrollParent[0].offsetWidth) - event.pageX < o.scrollSensitivity) {                // 1772
					i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft + o.scrollSpeed;                         // 1773
				} else if(event.pageX - i.overflowOffset.left < o.scrollSensitivity) {                                           // 1774
					i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft - o.scrollSpeed;                         // 1775
				}                                                                                                                // 1776
			}                                                                                                                 // 1777
                                                                                                                     // 1778
		} else {                                                                                                           // 1779
                                                                                                                     // 1780
			if(!o.axis || o.axis !== "x") {                                                                                   // 1781
				if(event.pageY - $(document).scrollTop() < o.scrollSensitivity) {                                                // 1782
					scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed);                                      // 1783
				} else if($(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity) {                  // 1784
					scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed);                                      // 1785
				}                                                                                                                // 1786
			}                                                                                                                 // 1787
                                                                                                                     // 1788
			if(!o.axis || o.axis !== "y") {                                                                                   // 1789
				if(event.pageX - $(document).scrollLeft() < o.scrollSensitivity) {                                               // 1790
					scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed);                                    // 1791
				} else if($(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity) {                  // 1792
					scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed);                                    // 1793
				}                                                                                                                // 1794
			}                                                                                                                 // 1795
                                                                                                                     // 1796
		}                                                                                                                  // 1797
                                                                                                                     // 1798
		if(scrolled !== false && $.ui.ddmanager && !o.dropBehaviour) {                                                     // 1799
			$.ui.ddmanager.prepareOffsets(i, event);                                                                          // 1800
		}                                                                                                                  // 1801
                                                                                                                     // 1802
	}                                                                                                                   // 1803
});                                                                                                                  // 1804
                                                                                                                     // 1805
$.ui.plugin.add("draggable", "snap", {                                                                               // 1806
	start: function() {                                                                                                 // 1807
                                                                                                                     // 1808
		var i = $(this).data("ui-draggable"),                                                                              // 1809
			o = i.options;                                                                                                    // 1810
                                                                                                                     // 1811
		i.snapElements = [];                                                                                               // 1812
                                                                                                                     // 1813
		$(o.snap.constructor !== String ? ( o.snap.items || ":data(ui-draggable)" ) : o.snap).each(function() {            // 1814
			var $t = $(this),                                                                                                 // 1815
				$o = $t.offset();                                                                                                // 1816
			if(this !== i.element[0]) {                                                                                       // 1817
				i.snapElements.push({                                                                                            // 1818
					item: this,                                                                                                     // 1819
					width: $t.outerWidth(), height: $t.outerHeight(),                                                               // 1820
					top: $o.top, left: $o.left                                                                                      // 1821
				});                                                                                                              // 1822
			}                                                                                                                 // 1823
		});                                                                                                                // 1824
                                                                                                                     // 1825
	},                                                                                                                  // 1826
	drag: function(event, ui) {                                                                                         // 1827
                                                                                                                     // 1828
		var ts, bs, ls, rs, l, r, t, b, i, first,                                                                          // 1829
			inst = $(this).data("ui-draggable"),                                                                              // 1830
			o = inst.options,                                                                                                 // 1831
			d = o.snapTolerance,                                                                                              // 1832
			x1 = ui.offset.left, x2 = x1 + inst.helperProportions.width,                                                      // 1833
			y1 = ui.offset.top, y2 = y1 + inst.helperProportions.height;                                                      // 1834
                                                                                                                     // 1835
		for (i = inst.snapElements.length - 1; i >= 0; i--){                                                               // 1836
                                                                                                                     // 1837
			l = inst.snapElements[i].left;                                                                                    // 1838
			r = l + inst.snapElements[i].width;                                                                               // 1839
			t = inst.snapElements[i].top;                                                                                     // 1840
			b = t + inst.snapElements[i].height;                                                                              // 1841
                                                                                                                     // 1842
			if ( x2 < l - d || x1 > r + d || y2 < t - d || y1 > b + d || !$.contains( inst.snapElements[ i ].item.ownerDocument, inst.snapElements[ i ].item ) ) {
				if(inst.snapElements[i].snapping) {                                                                              // 1844
					(inst.options.snap.release && inst.options.snap.release.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item })));
				}                                                                                                                // 1846
				inst.snapElements[i].snapping = false;                                                                           // 1847
				continue;                                                                                                        // 1848
			}                                                                                                                 // 1849
                                                                                                                     // 1850
			if(o.snapMode !== "inner") {                                                                                      // 1851
				ts = Math.abs(t - y2) <= d;                                                                                      // 1852
				bs = Math.abs(b - y1) <= d;                                                                                      // 1853
				ls = Math.abs(l - x2) <= d;                                                                                      // 1854
				rs = Math.abs(r - x1) <= d;                                                                                      // 1855
				if(ts) {                                                                                                         // 1856
					ui.position.top = inst._convertPositionTo("relative", { top: t - inst.helperProportions.height, left: 0 }).top - inst.margins.top;
				}                                                                                                                // 1858
				if(bs) {                                                                                                         // 1859
					ui.position.top = inst._convertPositionTo("relative", { top: b, left: 0 }).top - inst.margins.top;              // 1860
				}                                                                                                                // 1861
				if(ls) {                                                                                                         // 1862
					ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l - inst.helperProportions.width }).left - inst.margins.left;
				}                                                                                                                // 1864
				if(rs) {                                                                                                         // 1865
					ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r }).left - inst.margins.left;           // 1866
				}                                                                                                                // 1867
			}                                                                                                                 // 1868
                                                                                                                     // 1869
			first = (ts || bs || ls || rs);                                                                                   // 1870
                                                                                                                     // 1871
			if(o.snapMode !== "outer") {                                                                                      // 1872
				ts = Math.abs(t - y1) <= d;                                                                                      // 1873
				bs = Math.abs(b - y2) <= d;                                                                                      // 1874
				ls = Math.abs(l - x1) <= d;                                                                                      // 1875
				rs = Math.abs(r - x2) <= d;                                                                                      // 1876
				if(ts) {                                                                                                         // 1877
					ui.position.top = inst._convertPositionTo("relative", { top: t, left: 0 }).top - inst.margins.top;              // 1878
				}                                                                                                                // 1879
				if(bs) {                                                                                                         // 1880
					ui.position.top = inst._convertPositionTo("relative", { top: b - inst.helperProportions.height, left: 0 }).top - inst.margins.top;
				}                                                                                                                // 1882
				if(ls) {                                                                                                         // 1883
					ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l }).left - inst.margins.left;           // 1884
				}                                                                                                                // 1885
				if(rs) {                                                                                                         // 1886
					ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r - inst.helperProportions.width }).left - inst.margins.left;
				}                                                                                                                // 1888
			}                                                                                                                 // 1889
                                                                                                                     // 1890
			if(!inst.snapElements[i].snapping && (ts || bs || ls || rs || first)) {                                           // 1891
				(inst.options.snap.snap && inst.options.snap.snap.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item })));
			}                                                                                                                 // 1893
			inst.snapElements[i].snapping = (ts || bs || ls || rs || first);                                                  // 1894
                                                                                                                     // 1895
		}                                                                                                                  // 1896
                                                                                                                     // 1897
	}                                                                                                                   // 1898
});                                                                                                                  // 1899
                                                                                                                     // 1900
$.ui.plugin.add("draggable", "stack", {                                                                              // 1901
	start: function() {                                                                                                 // 1902
		var min,                                                                                                           // 1903
			o = this.data("ui-draggable").options,                                                                            // 1904
			group = $.makeArray($(o.stack)).sort(function(a,b) {                                                              // 1905
				return (parseInt($(a).css("zIndex"),10) || 0) - (parseInt($(b).css("zIndex"),10) || 0);                          // 1906
			});                                                                                                               // 1907
                                                                                                                     // 1908
		if (!group.length) { return; }                                                                                     // 1909
                                                                                                                     // 1910
		min = parseInt($(group[0]).css("zIndex"), 10) || 0;                                                                // 1911
		$(group).each(function(i) {                                                                                        // 1912
			$(this).css("zIndex", min + i);                                                                                   // 1913
		});                                                                                                                // 1914
		this.css("zIndex", (min + group.length));                                                                          // 1915
	}                                                                                                                   // 1916
});                                                                                                                  // 1917
                                                                                                                     // 1918
$.ui.plugin.add("draggable", "zIndex", {                                                                             // 1919
	start: function(event, ui) {                                                                                        // 1920
		var t = $(ui.helper), o = $(this).data("ui-draggable").options;                                                    // 1921
		if(t.css("zIndex")) {                                                                                              // 1922
			o._zIndex = t.css("zIndex");                                                                                      // 1923
		}                                                                                                                  // 1924
		t.css("zIndex", o.zIndex);                                                                                         // 1925
	},                                                                                                                  // 1926
	stop: function(event, ui) {                                                                                         // 1927
		var o = $(this).data("ui-draggable").options;                                                                      // 1928
		if(o._zIndex) {                                                                                                    // 1929
			$(ui.helper).css("zIndex", o._zIndex);                                                                            // 1930
		}                                                                                                                  // 1931
	}                                                                                                                   // 1932
});                                                                                                                  // 1933
                                                                                                                     // 1934
})(jQuery);                                                                                                          // 1935
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                     // 1945
}).call(this);                                                       // 1946
                                                                     // 1947
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['dbernhard:jquery-ui-draggable'] = {};

})();
