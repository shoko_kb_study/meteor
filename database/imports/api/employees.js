import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Employees = new Meteor.Collection('employees');

Meteor.methods({
    // 初期化
    'employees.init'() {
        // Employees.remove({});

        // // 決め打ちでデータを登録する。
        // var data = [
        //     { name: 'しゅんぺい', age: '34' },
        //     { name: 'たえこ', age: '33' },
        //     { name: 'こうたろう', age: '4' },
        //     { name: 'ちほ', age: '2' }
        // ];
        // // データをMongoDBに挿入する。
        // data.forEach(function (emp) {
        //     Employees.insert(emp);
        // })
    },
    // 追加
    'employees.insert'(name, age) {
		check(name, String);
		check(age, String);
		//console.log(name, age);
		Employees.insert({name: name, age :age});
    },
    // 編集
    'employees.update' (id, name, age) {
    	const employee = Employees.findOne(id);
    	console.log(employee)
    	console.log('employees.update', id, name, age);
	    Employees.update(id, { $set: { name: name, age: age } });

    },
    // 削除
    'employees.remove'(employeeId) {
        check(employeeId, String);
        const employee = Employees.findOne(employeeId);
        Employees.remove(employeeId);
    },
});