import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.family.helpers({
	person: { name: 'Shoko JP' },
	family : {
		lastname: 'JP',
		mother: { name:'Hanako' },
		father: { name:'Taro' }
	},
});

// Template.family.isChild = function () {
// 	return
// };

Template.current_time.helpers({
	now: new Date(),
	oddSeconds: function () {
		return (new Date()).getSeconds() % 2 === 1;
	},
	dateString: function () {
      var date = new Date();
      var date_ja = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日 ' +
      	date.getHours() + '時' + date.getMinutes() + '分' + date.getSeconds() + '秒';
      return date_ja;
	}
});
