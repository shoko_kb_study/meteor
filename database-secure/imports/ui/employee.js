import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './employee.html';

Template.employee.events({
    'click .edit'(event) {
    	const target = event.target;
    	const name = $(target).parents('tr').find('.name').val();
    		// this.nameではDBに登録された値が取得される。
    	const age = $(target).parents('tr').find('.age').val();
        Meteor.call('employees.update', this._id, name, age);
    },
    'click .del'() {
        if (confirm('削除しますか?')) {
            Meteor.call('employees.remove', this._id);
        }
    },
});