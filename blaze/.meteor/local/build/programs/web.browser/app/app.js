var require = meteorInstall({"client":{"main.html":["./template.main.js",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                           //
// client/main.html                                                                          //
//                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////
                                                                                             //
module.exports = require("./template.main.js");                                              // 1
                                                                                             // 2
///////////////////////////////////////////////////////////////////////////////////////////////

}],"template.main.js":function(){

///////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                           //
// client/template.main.js                                                                   //
//                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////
                                                                                             //
                                                                                             // 1
Template.body.addContent((function() {                                                       // 2
  var view = this;                                                                           // 3
  return [ HTML.Raw("<h1>Welcome to Meteor!</h1>\n\n  "), Spacebars.include(view.lookupTemplate("family")), HTML.Raw("\n\n  <h2>ifで記述</h2>\n  "), HTML.P(Spacebars.include(view.lookupTemplate("current_time"))), HTML.Raw("\n\n  <h2>unlessで記述</h2>\n  "), HTML.P(Spacebars.include(view.lookupTemplate("current_time_unless"))), HTML.Raw("\n\n  <h2>eachで記述（デフォルトの昇順ループ）</h2>\n  "), Spacebars.include(view.lookupTemplate("fruits")), HTML.Raw("\n\n  <h2>letで記述</h2>\n  "), HTML.P(Spacebars.include(view.lookupTemplate("sample_let"))), HTML.Raw("\n\n  <h2>自作 Block Helper（降順ループ）</h2>\n  "), Spacebars.include(view.lookupTemplate("fruits_reverse")), "\n\n  ", Spacebars.include(view.lookupTemplate("caller_myif")) ];
}));                                                                                         // 5
Meteor.startup(Template.body.renderToDocument);                                              // 6
                                                                                             // 7
Template.__checkName("family");                                                              // 8
Template["family"] = new Template("Template.family", (function() {                           // 9
  var view = this;                                                                           // 10
  return [ HTML.P("ようこそ、", Blaze.View("lookup:person.name", function() {                     // 11
    return Spacebars.mustache(Spacebars.dot(view.lookup("person"), "name"));                 // 12
  }), "."), "\n  ", Spacebars.With(function() {                                              // 13
    return Spacebars.call(view.lookup("family"));                                            // 14
  }, function() {                                                                            // 15
    return [ "\n    ", HTML.P("苗字は", Blaze.View("lookup:lastname", function() {              // 16
      return Spacebars.mustache(view.lookup("lastname"));                                    // 17
    }), "。"), "\n    ", Spacebars.With(function() {                                          // 18
      return Spacebars.call(view.lookup("mother"));                                          // 19
    }, function() {                                                                          // 20
      return [ "\n      ", HTML.P("母は、", Blaze.View("lookup:name", function() {              // 21
        return Spacebars.mustache(view.lookup("name"));                                      // 22
      })), "\n      ", HTML.P("父は、", Blaze.View("lookup:...father.name", function() {        // 23
        return Spacebars.mustache(Spacebars.dot(view.lookup(".."), "father", "name"));       // 24
      })), "\n    " ];                                                                       // 25
    }), "\n  " ];                                                                            // 26
  }) ];                                                                                      // 27
}));                                                                                         // 28
                                                                                             // 29
Template.__checkName("current_time");                                                        // 30
Template["current_time"] = new Template("Template.current_time", (function() {               // 31
  var view = this;                                                                           // 32
  return Blaze.If(function() {                                                               // 33
    return Spacebars.call(view.lookup("oddSeconds"));                                        // 34
  }, function() {                                                                            // 35
    return [ "\n    現在時刻は", Blaze.View("lookup:dateString", function() {                     // 36
      return Spacebars.mustache(view.lookup("dateString"));                                  // 37
    }), "\n  " ];                                                                            // 38
  }, function() {                                                                            // 39
    return [ "\n    現在時刻は", Blaze.View("lookup:now", function() {                            // 40
      return Spacebars.mustache(view.lookup("now"));                                         // 41
    }), "\n  " ];                                                                            // 42
  });                                                                                        // 43
}));                                                                                         // 44
                                                                                             // 45
Template.__checkName("current_time_unless");                                                 // 46
Template["current_time_unless"] = new Template("Template.current_time_unless", (function() {
  var view = this;                                                                           // 48
  return Blaze.Unless(function() {                                                           // 49
    return Spacebars.call(view.lookup("oddSeconds"));                                        // 50
  }, function() {                                                                            // 51
    return [ "\n    ", HTML.SPAN({                                                           // 52
      style: "color:green"                                                                   // 53
    }, "現在時刻は", Blaze.View("lookup:dateString", function() {                                 // 54
      return Spacebars.mustache(view.lookup("dateString"));                                  // 55
    })), "\n  " ];                                                                           // 56
  }, function() {                                                                            // 57
    return [ "\n    ", HTML.SPAN({                                                           // 58
      style: "color:red"                                                                     // 59
    }, "現在時刻は", Blaze.View("lookup:now", function() {                                        // 60
      return Spacebars.mustache(view.lookup("now"));                                         // 61
    })), "\n  " ];                                                                           // 62
  });                                                                                        // 63
}));                                                                                         // 64
                                                                                             // 65
Template.__checkName("fruits");                                                              // 66
Template["fruits"] = new Template("Template.fruits", (function() {                           // 67
  var view = this;                                                                           // 68
  return HTML.UL("\n    ", Blaze.Each(function() {                                           // 69
    return Spacebars.call(view.lookup("fruits"));                                            // 70
  }, function() {                                                                            // 71
    return [ "\n      ", HTML.LI(Blaze.View("lookup:.", function() {                         // 72
      return Spacebars.mustache(view.lookup("."));                                           // 73
    })), "\n    " ];                                                                         // 74
  }), "\n  ");                                                                               // 75
}));                                                                                         // 76
                                                                                             // 77
Template.__checkName("sample_let");                                                          // 78
Template["sample_let"] = new Template("Template.sample_let", (function() {                   // 79
  var view = this;                                                                           // 80
  return Blaze.Let({                                                                         // 81
    name: function() {                                                                       // 82
      return Spacebars.call(Spacebars.dot(view.lookup("person"), "bio", "firstName"));       // 83
    },                                                                                       // 84
    color: function() {                                                                      // 85
      return Spacebars.call(view.lookup("generateColor"));                                   // 86
    }                                                                                        // 87
  }, function() {                                                                            // 88
    return [ "\n    ", HTML.DIV(Blaze.View("lookup:name", function() {                       // 89
      return Spacebars.mustache(view.lookup("name"));                                        // 90
    }), " gets a ", Blaze.View("lookup:color", function() {                                  // 91
      return Spacebars.mustache(view.lookup("color"));                                       // 92
    }), " card!"), "\n  " ];                                                                 // 93
  });                                                                                        // 94
}));                                                                                         // 95
                                                                                             // 96
Template.__checkName("fruits_reverse");                                                      // 97
Template["fruits_reverse"] = new Template("Template.fruits_reverse", (function() {           // 98
  var view = this;                                                                           // 99
  return HTML.UL("\n    ", Blaze.Each(function() {                                           // 100
    return Spacebars.call(view.lookup("fruits"));                                            // 101
  }, function() {                                                                            // 102
    return [ "\n      ", HTML.LI(Blaze.View("lookup:.", function() {                         // 103
      return Spacebars.mustache(view.lookup("."));                                           // 104
    })), "\n    " ];                                                                         // 105
  }), "\n  ");                                                                               // 106
}));                                                                                         // 107
                                                                                             // 108
Template.__checkName("myIf");                                                                // 109
Template["myIf"] = new Template("Template.myIf", (function() {                               // 110
  var view = this;                                                                           // 111
  return Blaze.If(function() {                                                               // 112
    return Spacebars.call(view.lookup("condition"));                                         // 113
  }, function() {                                                                            // 114
    return [ "\n    ", Blaze._InOuterTemplateScope(view, function() {                        // 115
      return Spacebars.include(function() {                                                  // 116
        return Spacebars.call(view.templateContentBlock);                                    // 117
      });                                                                                    // 118
    }), "\n  " ];                                                                            // 119
  }, function() {                                                                            // 120
    return [ "\n    ", Blaze._InOuterTemplateScope(view, function() {                        // 121
      return Spacebars.include(function() {                                                  // 122
        return Spacebars.call(view.templateElseBlock);                                       // 123
      });                                                                                    // 124
    }), "\n  " ];                                                                            // 125
  });                                                                                        // 126
}));                                                                                         // 127
                                                                                             // 128
Template.__checkName("caller_myif");                                                         // 129
Template["caller_myif"] = new Template("Template.caller_myif", (function() {                 // 130
  var view = this;                                                                           // 131
  return Blaze._TemplateWith(function() {                                                    // 132
    return {                                                                                 // 133
      condition: Spacebars.call(true)                                                        // 134
    };                                                                                       // 135
  }, function() {                                                                            // 136
    return Spacebars.include(view.lookupTemplate("myIf"), function() {                       // 137
      return [ "\n    ", HTML.H1("I'll be rendered!"), "\n  " ];                             // 138
    }, function() {                                                                          // 139
      return [ "\n    ", HTML.H1("I won't be rendered"), "    \n  " ];                       // 140
    });                                                                                      // 141
  });                                                                                        // 142
}));                                                                                         // 143
                                                                                             // 144
///////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":["meteor/templating","meteor/reactive-var","./main.html",function(require,exports,module){

///////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                           //
// client/main.js                                                                            //
//                                                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////
                                                                                             //
var Template;module.import('meteor/templating',{"Template":function(v){Template=v}});var ReactiveVar;module.import('meteor/reactive-var',{"ReactiveVar":function(v){ReactiveVar=v}});module.import('./main.html');
                                                                                             // 2
                                                                                             //
                                                                                             // 4
                                                                                             //
Template.family.helpers({                                                                    // 6
	person: { name: 'Shoko JP' },                                                               // 7
	family: {                                                                                   // 8
		lastname: 'JP',                                                                            // 9
		mother: { name: 'Hanako' },                                                                // 10
		father: { name: 'Taro' }                                                                   // 11
	}                                                                                           // 8
});                                                                                          // 6
                                                                                             //
Template.current_time.helpers({                                                              // 15
	now: new Date(),                                                                            // 16
	oddSeconds: function () {                                                                   // 17
		function oddSeconds() {                                                                    // 17
			var date = new Date();                                                                    // 18
			//console.log(date.getSeconds());                                                         //
			return date.getSeconds() % 2 === 1;                                                       // 20
		}                                                                                          // 21
                                                                                             //
		return oddSeconds;                                                                         // 17
	}(),                                                                                        // 17
	dateString: function () {                                                                   // 22
		function dateString() {                                                                    // 22
			var date = new Date();                                                                    // 23
			var date_ja = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日 ' + date.getHours() + '時' + date.getMinutes() + '分' + date.getSeconds() + '秒';
			return date_ja;                                                                           // 26
		}                                                                                          // 27
                                                                                             //
		return dateString;                                                                         // 22
	}()                                                                                         // 22
});                                                                                          // 15
                                                                                             //
Template.current_time_unless.helpers({                                                       // 30
	now: new Date(),                                                                            // 31
	oddSeconds: function () {                                                                   // 32
		function oddSeconds() {                                                                    // 32
			var date = new Date();                                                                    // 33
			//console.log(date.getSeconds());                                                         //
			return date.getSeconds() % 2 === 1;                                                       // 35
		}                                                                                          // 36
                                                                                             //
		return oddSeconds;                                                                         // 32
	}(),                                                                                        // 32
	dateString: function () {                                                                   // 37
		function dateString() {                                                                    // 37
			var date = new Date();                                                                    // 38
			var date_ja = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日 ' + date.getHours() + '時' + date.getMinutes() + '分' + date.getSeconds() + '秒';
			return date_ja;                                                                           // 41
		}                                                                                          // 42
                                                                                             //
		return dateString;                                                                         // 37
	}()                                                                                         // 37
});                                                                                          // 30
                                                                                             //
Template.fruits.helpers({                                                                    // 45
	fruits: ['apple', 'banana', 'orange']                                                       // 46
});                                                                                          // 45
                                                                                             //
Template.fruits_reverse.helpers({                                                            // 49
	fruits: ['apple', 'banana', 'orange']                                                       // 50
});                                                                                          // 49
                                                                                             //
//　<<自作 Block Helper>>                                                                       //
Handlebars.registerHelper('reverseEach', function (items, options) {                         // 54
	var contents = [];                                                                          // 55
	// 関数を逆順で処理する                                                                               //
	for (var i = items.length - 1; i >= 0; i--) {                                               // 57
		contents.push(options.fn(items[i]));                                                       // 58
	}                                                                                           // 59
	// テンプレート内部を実行した結果を，文字列として返す                                                                //
	return contents.join('');                                                                   // 61
});                                                                                          // 62
                                                                                             //
Template.sample_let.helpers({                                                                // 64
	person: {                                                                                   // 65
		bio: { firstName: 'Shoko' }                                                                // 66
	},                                                                                          // 65
	generateColor: 'red'                                                                        // 68
});                                                                                          // 64
///////////////////////////////////////////////////////////////////////////////////////////////

}]}},{"extensions":[".js",".json",".html",".css"]});
require("./client/template.main.js");
require("./client/main.js");