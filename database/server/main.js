import { Employees } from '../imports/api/employees.js';

Meteor.startup(() => {
	Meteor.call('employees.init');
});
