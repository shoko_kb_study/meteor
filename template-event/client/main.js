import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.mainContent.helpers({
});

Template.mainContent.events({
  'click button .greetings'(event, template) {
    alert("Button Clicked");

    console.log(template);

    console.log(template.firstNode);
    console.log(template.lastNode);
    console.log(template.findAll('button'));	// 検索にヒットするすべてのノード
    console.log(template.find('button'));		// 検索にヒットする最初のノード

    console.log(template.$('button'));			// jquery

    // console.log(template.data());					// テンプレートが呼び出された際に渡されたデータ
  },
  'click #hello' (event, template) {
    alert("Hello");
  },
  'click #goodMorning' (event, template) {
    alert("Good Morning");
  },
  'click #greet' (event, template) {
    var name = template.$('input[type=text]').val();      //jquery
    //var name = template.find('input[type=text]').value; //DOM
    console.log(name);
  }
});

Template.mainContent.onCreated(function () {
	console.log('created');

	var template = this;
	console.log(template);
});
Template.mainContent.onRendered(function() {
    console.log('rendered');
});
Template.mainContent.onDestroyed(function() {
    console.log('destroyed');
});