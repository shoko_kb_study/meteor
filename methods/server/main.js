import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({
	hello : function (firstname, lastname) {
		return "Hello, " + firstname + " " + lastname;
	}
});
