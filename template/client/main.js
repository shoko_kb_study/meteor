import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Meteor.startup(() => {
	console.log(Template.sample1);
	console.log(Template.sample2);
	console.log(Template.welcome);
});

Template.welcomeja.helpers({
  yourname: 'Shoko',
  now: new Date()
});
