import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
	// startup is only server-side
	if (Meteor.is_client) {
		console.log("client-side");
	} else {
		console.log("server-side");
	}
});
