import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { Tasks } from '../api/tasks.js';

import Task from './Task.jsx';

// アプリケーション全体に提示されるApp Component
// App component - represents the whole app
class App extends Component {
// export default class App extends Component {

  // getTasks() {
  //   // JSONとして返却する。
  //   return [
  //     { _id: 1, text: 'This is task 1' },
  //     { _id: 2, text: 'This is task 2' },
  //     { _id: 3, text: 'This is task 3' },
  //   ];
  // }

  // Taskを取得し、map関数でTaskを表示する。
  renderTasks() {
    // (3)App(this).propsに対して、Task Componentを表示する。
    console.log(this.props.tasks);
    return this.props.tasks.map((task) => (
    // return this.getTasks().map((task) => (
      <Task key={task._id} task={task} />
    ));
  }

  render() {
    return (
      <div className="container">
        <header>
          <h1>Todo List</h1>
        </header>

        <ul>
          {this.renderTasks()}
        </ul>
      </div>
    );
  }
}

App.propTypes = {
  // (2) App(this).props.tasks
  tasks: PropTypes.array.isRequired,
};

export default createContainer(() => {
  // (1)tasksにDBのTasks(tasks)の全データを取得し、フェッチする
  return {
    tasks: Tasks.find({}).fetch(),
  };
}, App);
